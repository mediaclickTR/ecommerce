<?php

return [
    "order_number" => 'Order No',
    "currency" => "Currency",
    "total" => "Total",
    "sort" => "Order",
    "show" => "Show",
    "delete" => "Delete",
    "total_items" => "Items Count",
    "status" => [
        "payment_waiting" => "Waiting for Payment",
        "process" => "In process",
        "cancel" => "Cancel",
        "invalid" => "Invalid",
        "success" => "Completed",
    ],
    "detail" => "Detail",
    "deleteOption" => [
        "success" => "Deletion is successful",
        "error" => "An error occurred during the deletion",
    ],
    "order" => [
        "status" => "Order Status",
        "info" => "Ordering Information",
        "detail" => "Order Details",
        "total" => "Order Total",
    ],
    "excel_export" => "Excel Export",
    "order_date" => 'Order Date',
    "order_name" => 'Order Name/Surname',
    "payment_credit" => 'Credit Card',
    "transfer" => "Bank Transfer",
    "payment_type_text" => "Payment Type",
    "status_shipping" => "Shipping",
    "plac_order" => "İsim, Soyisim ya da Sipariş No.",
    "all" => "All",
    "search_by_key" => "Search by key...",
    "filter" => [
        "price" => "Min. - Max. Price",
        "min_price" => "Min. Price",
        "max_price" => "Max. Price",
        "payment_type" => "Payment Type",
        "credit" => "Credit Cart",
        "transfer" => "Transfer",
        "date" => "Order date range",
        "start_date" => "Start Date",
        "end_date" => "End Date",
        "search" => "Search Orders",
        "clear" => "Clear"
    ]

];
