<?php

return [
    "order_number" => 'Sipariş No',
    "currency" => "Para Birimi",
    "total" => "Toplam",
    "sort" => "Sıra",
    "show" => "Görüntüle",
    "delete" => "Sil",
    "total_items" => "Sipariş İçeriği",
    "status" => [
        "payment_waiting" => "Ödeme Bekleniyor",
        "process" => "İşlemde",
        "cancel" => "İptal",
        "invalid" => "Geçersiz",
        "success" => "Tamamlandı",
    ],
    "detail" => "Detay",
    "deleteOption" => [
      "success" => "Silme işlemi başarılı",
        "error" => "Silme işlemi sırasında bir hata oluştu",
    ],
    "order" => [
        "status" => "Sipariş Durumu",
        "info" => "Sipariş Bilgileri",
        "detail" => "Sipariş Detayları",
        "total" => "Sipariş Toplamı",
    ],
    "excel_export" => 'Excel Çıktısı Al',
    "order_date" => 'Sipariş Tarihi',
    "order_name" => 'Sipariş Ad/Soyad',
    "payment_credit" => 'Kredi Kartı',
    "transfer" => "Havale/EFT",
    "payment_type_text" => "Ödeme Tipi",
    "status_shipping" => "Kargoya Verildi",
    "plac_order" => "İsim, Soyisim ya da Sipariş No.",
    "all" => "Tümü",
    "search_by_key" => "Anahtar kelime ile ara...",
    "filter" => [
        "price" => "Min. - Max. Tutar",
        "min_price" => "Min. Ücret",
        "max_price" => "Max. Ücret",
        "payment_type" => "Ödeme Tipi",
        "credit" => "Kredi Kartı",
        "transfer" => "Havale/EFT",
        "date" => "Sipariş tarih aralığı",
        "start_date" => "Başlangıç Tarihi",
        "end_date" => "Bitiş Tarihi",
        "search" => "Sipariş Ara",
        "clear" => "Temizle"
    ]
];
