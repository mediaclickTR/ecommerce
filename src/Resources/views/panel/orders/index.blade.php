@extends('MPCorePanel::inc.app')

@section('content')
    <style type="text/css">
        .breadcrumb ul>li>a{
            max-width: 300px;
        }
    </style>
    <div class="breadcrumb">
        <div class="float-left">
            <ul>
                <li data-key="dashboard">
                    <a href="/mp-admin" title="Dashboard" class="text-truncate"><i class="fa fa-home"></i> Dashboard</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="javascript:void(0);" title="{!! trans('ECommercePanel::menu_titles.name') !!}" class="text-truncate">
                        <i class="fa fa-sitemap"></i>{!! trans('ECommercePanel::menu_titles.name') !!}
                    </a>
                </li>
            </ul>
        </div>
    </div>
    @include('ECommerceView::orders.filter')
    <div class="page-content">
        @include('MPCorePanel::inc.errors')
        <div class="row">
            <div class=" col-sm-6 float-left">
                <div class="title"> {!! trans('ECommercePanel::menu_titles.name') !!}</div>
            </div>
            @if(config('ecommerce.excel.export'))
            <div class=" col-sm-6 float-right">
                <div class="title text-right">
                    <a href="{!! route('Orders.excel.export') !!}" class="btn btn-outline-success">{!! __('ECommercePanel::general.excel_export') !!}</a>
                </div>
            </div>
            @endif
        </div>
        <div class="clearfix"></div>

        <div class="table-field mt-5 all-results">
            {!! $dataTable->table() !!}
        </div>
    </div>
@endsection
