<div class="page-content mb-3">
    <form action="?" method="GET">
        <div class="row">
            <div class="col-lg-12">
                <div class="title"> {!! trans('ECommercePanel::menu_titles.filter_name') !!}</div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-lg-3">
                <label>{!! trans('ECommercePanel::general.search_by_key') !!}</label>
                <input type="text" name="key" class="form-control form-control-sm" placeholder="{!! trans('ECommercePanel::general.plac_order') !!}" value="{!! \Request::has('key') && \Request::filled('key') ? \Request::get('key') : '' !!}"/>
            </div>
            <div class="col-lg-3">
                <label>{!! trans('ECommercePanel::general.order.status') !!}</label>
                <select name="status" class="form-control form-control-sm">
                    <option value="">{!! trans('ECommercePanel::general.all') !!}</option>
                    @if( function_exists('set_order_status') )
                        @foreach( set_order_status() as $status )
                            <option value="{!! $status['id'] !!}" {!! \Request::has('status') && \Request::get('status') == $status['id'] ? 'selected' : '' !!}>{!! $status['text'] !!}</option>
                        @endforeach
                    @else
                        <option value="1" {!! \Request::has('status') && \Request::get('status') == 1 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.status.payment_waiting') !!}</option>
                        <option value="2" {!! \Request::has('status') && \Request::get('status') == 2 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.status.process') !!}</option>
                        <option value="3" {!! \Request::has('status') && \Request::get('status') == 3 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.status.cancel') !!}</option>
                        <option value="4" {!! \Request::has('status') && \Request::get('status') == 4 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.status.invalid') !!}</option>
                        <option value="5" {!! \Request::has('status') && \Request::get('status') == 5 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.status_shipping') !!}</option>
                        <option value="6" {!! \Request::has('status') && \Request::get('status') == 6 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.status.success') !!}</option>
                    @endif

                </select>
            </div>
            <div class="col-lg-3">
                <label>{!! trans('ECommercePanel::general.filter.price') !!}</label>
                <div class="row">
                    <div class="col-lg-6">
                        <input type="text" name="min_price" class="form-control form-control-sm" placeholder="{!! trans('ECommercePanel::general.filter.min_price') !!}" value="{!! \Request::has('min_price') && \Request::filled('min_price') ? \Request::get('min_price') : '' !!}"/>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="max_price" class="form-control form-control-sm" placeholder="{!! trans('ECommercePanel::general.filter.max_price') !!}" value="{!! \Request::has('max_price') && \Request::filled('max_price') ? \Request::get('max_price') : '' !!}"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <label>{!! trans('ECommercePanel::general.filter.payment_type') !!}</label>
                <select name="payment_type" class="form-control form-control-sm">
                    <option value="">{!! trans('ECommercePanel::general.all') !!}</option>
                    <option value="1" {!! \Request::has('payment_type') && \Request::get('payment_type') == 1 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.filter.credit') !!}</option>
                    <option value="2" {!! \Request::has('payment_type') && \Request::get('payment_type') == 2 ? 'selected' : '' !!}>{!! trans('ECommercePanel::general.filter.transfer') !!}</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label>{!! trans('ECommercePanel::general.filter.date') !!}</label>
                <div class="row">
                    <div class="col-lg-6">
                        <input type="text" name="start_date" class="form-control form-control-sm datepicker" placeholder="{!! trans('ECommercePanel::general.filter.start_date') !!}" value="{!! \Request::has('start_date') && \Request::filled('start_date') ? \Request::get('start_date') : '' !!}" autocomplete="off"/>
                    </div>
                    <div class="col-lg-6">
                        <input type="text" name="end_date" class="form-control form-control-sm datepicker" placeholder="{!! trans('ECommercePanel::general.filter.end_date') !!}" value="{!! \Request::has('end_date') && \Request::filled('end_date') ? \Request::get('end_date') : '' !!}" autocomplete="off"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 mr-3">
                <button type="submit" class="btn btn-success btn-sm" style="margin-top: 1.9rem!important;">{!! trans('ECommercePanel::general.filter.search') !!}</button>
            </div>
            <div class="col-lg-1">
                <a href="{!! Route('Orders.index') !!}" class="btn btn-danger btn-sm" style="margin-top: 1.9rem!important;">{!! trans('ECommercePanel::general.filter.clear') !!}</a>
            </div>
        </div>
    </form>
</div>
