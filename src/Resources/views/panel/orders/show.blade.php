@extends('MPCorePanel::inc.app')

@section("content")
    <style type="text/css">
        .breadcrumb ul > li > a {
            max-width: 300px;
        }
    </style>
    <div class="breadcrumb">
        <div class="float-left">
            <ul>
                <li data-key="dashboard">
                    <a href="/mp-admin" title="Dashboard" class="text-truncate"><i class="fa fa-home"></i> Dashboard</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="{!! route('Orders.index') !!}" title="{!! trans('ECommercePanel::menu_titles.name') !!}"
                       class="text-truncate"><i
                                class="fa fa-sitemap"></i>{!! trans('ECommercePanel::menu_titles.name') !!}</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="javascript:void(0);"
                       title="{!! $order->order_number !!} - {!! trans('ECommercePanel::general.detail') !!}"
                       class="text-truncate">
                        <i class="fa fa-sitemap"></i>
                        {!! $order->order_number !!} - {!! trans('ECommercePanel::general.detail') !!}
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-content">
        <div class="float-left">
            <div class="title"> {!! $order->order_number !!} - {!! trans('ECommercePanel::general.detail') !!}</div>
        </div>

        <div class="clearfix"></div>
        @if( function_exists('set_order_status') )
            <h6 class="text-center" style="padding-bottom: 20px;">{!! trans('ECommercePanel::general.order.status') !!}</h6>
            <div class="row">
                @foreach( set_order_status() as $status )
                    <div class="col-md-3">
                        <div class="form-check checkbox">
                            <input class="form-check-input" type="radio" name="status" id="status_{!! $status['id'] !!}" value="{!! $status['id'] !!}" {{ $order->status == $status['id'] ? 'checked' : '' }}>
                            <label class="form-check-label" for="status_{!! $status['id'] !!}">
                                {!! $status['text'] !!}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <table class="table table-hover">
                <tr align="center">
                    <th colspan="5"> {!! trans('ECommercePanel::general.order.status') !!}</th>
                </tr>
                <tr>
                    <th>
                        <div class="form-check checkbox">
                            <input class="form-check-input" type="radio" name="status" id="status_1" value="1" {{ $order->status == 1 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status_1">
                                {!! langPart('order_status_1', 'Ödeme Bekliyor') !!}
                            </label>
                        </div>
                    </th>
                    <th>
                        <div class="form-check checkbox">
                            <input class="form-check-input" type="radio" name="status" id="status_2" value="2" {{ $order->status == 2 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status_2">
                                {!! langPart('order_status_2', 'İşlemde') !!}
                            </label>
                        </div>
                    </th>
                    <th>
                        <div class="form-check checkbox">
                            <input class="form-check-input" type="radio" name="status" id="status_3" value="3" {{ $order->status == 3 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status_3">
                                {!! langPart('order_status_3', 'İptal') !!}
                            </label>
                        </div>
                    </th>
                    <th>
                        <div class="form-check checkbox">
                            <input class="form-check-input" type="radio" name="status" id="status_4" value="4" {{ $order->status == 4 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status_4">
                                {!! langPart('order_status_4', 'Geçersiz') !!}
                            </label>
                        </div>
                    </th>
                    <th>
                        <div class="form-check checkbox">
                            <input class="form-check-input" type="radio" name="status" id="status_5" value="5" {{ $order->status == 5 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status_5">
                                {!! langPart('order_status_6', 'Kargoya Verildi') !!}
                            </label>
                        </div>
                    </th>
                    <th>
                        <div class="form-check checkbox">
                            <input class="form-check-input" type="radio" name="status" id="status_6" value="6" {{ $order->status == 6 ? 'checked' : '' }}>
                            <label class="form-check-label" for="status_6">
                                {!! langPart('order_status_5', 'Tamamlandı') !!}
                            </label>
                        </div>
                    </th>
                </tr>
            </table>
        @endif

        <table class="table table-hover">
            <tr align="center">
                <th colspan="3"> {!! trans('ECommercePanel::general.order.info') !!}</th>
            </tr>
            @foreach($user as $key=>$value)
                <tr>
                    <th>{!! $key !!}</th>
                    <th>:</th>
                    <td>{!! $value !!}</td>
                </tr>
            @endforeach
            @if( $order->status == 4 )
                <tr>
                    <th>Banka Hatası:</th>
                    <th>:</th>
                    <td>{!! $order->response !!}</td>
                </tr>
            @endif
        </table>
        @if( $order->payment_type_id != 1 )
            @php
                $payment_type = \Mediapress\ECommerce\Models\PaymentType::where('id', $order->payment_type_id)->first();
            @endphp
            <table class="table table-hover">
                <thead>
                <tr align="center">
                    <th colspan="2"><strong>{!! LangPart('Havale/EFT Ödeme Banka Bilgileri') !!}</strong></th>
                </tr>
                <tr>
                    <th>{!! LangPart('transfer_bankname', 'Banka Adı') !!}</th>
                    <th>{!! $payment_type->bank !!}</th>
                </tr>
                <tr>
                    <th>{!! LangPart('transfer_bankiban', 'Banka IBAN') !!}</th>
                    <th>{!! $payment_type->detail['iban'] !!}</th>
                </tr>
                </thead>
            </table>
        @endif
        <div class="row d-flex justify-content-around">
            <div class="col-12 text-center"><h3>{!! trans('ECommercePanel::general.order.detail') !!}</h3></div>
            @foreach($items as $item)
                <div class="card" style="width: 23rem;">
                    <ul class="list-group list-group-flush">
                        @foreach($item as $key=>$value)
                            @if($value)
                                <li class="list-group-item d-flex justify-content-between">
                                    <span class="text-left font-weight-bold ">{!! $key !!} : </span>
                                    <span class="text-right">{!! $value !!}</span>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            @endforeach

            @if( count($extra_data) > 0 )
                @foreach( $extra_data as $data )
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{!! $data['title'] !!}</h4>
                            </div>
                            <div class="card-body">
                                <table>
                                    <thead>
                                    <tr>
                                        @foreach( $data['columns']['thead'] as $thead )
                                            <th>{!! $thead !!}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        @foreach( $data['columns']['tbody'] as $tbody )
                                            <th>{!! $tbody !!}</th>
                                        @endforeach
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

            <div class="col-md-12">
                <div class="table-field mt-5">
                    <table class="table table-hover mt-2">
                        <tr align="center">
                            <th colspan="3">{!! trans('ECommercePanel::general.order.total') !!}</th>
                        </tr>
                        @foreach($total as $key=>$value)
                            <tr>
                                <th>{!! $key !!}</th>
                                <th>:</th>
                                <td>{!! $value !!}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('[name="status"]').on('ifChecked', function () {
            var self = $(this);
            var value = $(this).val();

            $('.checkbox label').removeClass('text-success');
            $.ajax({
                url: '{{ route('Orders.updateStatus', ['id' => $order->id]) }}',
                method: 'GET',
                data: {status: value},
                success: function (response) {
                    self.closest('.checkbox').find('label').addClass('text-success');
                    setTimeout(function () {
                        self.closest('.checkbox').find('label').removeClass('text-success');
                    }, 1000)
                }
            });


        });
    </script>
@endpush