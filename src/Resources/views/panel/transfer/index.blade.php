@extends('MPCorePanel::inc.app')

@section('content')
    <style type="text/css">
        .breadcrumb ul>li>a{
            max-width: 300px;
        }
    </style>
    <div class="breadcrumb">
        <div class="float-left">
            <ul>
                <li data-key="dashboard">
                    <a href="/mp-admin" title="Dashboard" class="text-truncate"><i class="fa fa-home"></i> Dashboard</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="javascript:void(0);" title="{!! trans('ECommercePanel::menu_titles.transfer') !!}" class="text-truncate"><i class="fa fa-sitemap"></i>{!! trans('ECommercePanel::menu_titles.transfer') !!}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-content">
        @include('MPCorePanel::inc.errors')
        <div class="row">
            <div class=" col-sm-6 float-left">
                <div class="title"> {!! trans('ECommercePanel::menu_titles.transfer') !!}</div>
            </div>
            <div class=" col-sm-6 float-right">
                <a href="{!! route('Transfer.create') !!}" class="btn btn-primary btn-sm float-right"> <i class="fa fa-plus"></i> {!! trans("ContentPanel::sitemap.add_page") !!}</a>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="table-field mt-5 all-results">

            {!! $dataTable->table() !!}

        </div>
    </div>
@endsection
