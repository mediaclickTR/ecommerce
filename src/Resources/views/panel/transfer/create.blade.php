@extends('MPCorePanel::inc.app')

@section('content')
    <style type="text/css">
        .breadcrumb ul > li > a {
            max-width: 300px;
        }
        input.form-control:focus{
            border-color: #ced4da;
            box-shadow:none ;
        }
    </style>
    <div class="breadcrumb">
        <div class="float-left">
            <ul>
                <li data-key="dashboard">
                    <a href="/mp-admin" title="Dashboard" class="text-truncate"><i class="fa fa-home"></i> Dashboard</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="{!! route('Transfer.index') !!}" title="{!! trans('ECommercePanel::menu_titles.transfer') !!}"
                       class="text-truncate"><i
                            class="fa fa-sitemap"></i>{!! trans('ECommercePanel::menu_titles.transfer') !!}</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="javascript:void(0)">Yeni Ekle</a>
                   </li>
            </ul>
        </div>
    </div>
    <div class="page-content">

        <div class="float-left">
            <div class="title"> Havale Bankası Ekle</div>
        </div>
        <form action="{!! route('Transfer.create') !!}" method="post">
            @csrf
            <div class="form-group">
                <label >
                    Banka
                </label>
                <select name="code" class="form-control" required>
                    <option value="">Seçiniz</option>
                    @foreach(config('ecommerce.banks') as $code=>$bank)
                        <option value="{{$code}}">{{$bank}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>
                    IBAN
                </label>
                <input type="text" name="iban" class="form-control" required>
            </div>
            <div class="form-group">

                <input type="submit"  class="form-control col-sm-2 btn btn-primary float-right" value="Kaydet">
            </div>
        </form>

        <div class="clearfix"></div>

    </div>
@endsection
