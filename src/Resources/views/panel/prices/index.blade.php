@extends('MPCorePanel::inc.app')

@section('content')
    <style type="text/css">
        .breadcrumb ul > li > a {
            max-width: 300px;
        }
    </style>
    <div class="breadcrumb">
        <div class="float-left">
            <ul>
                <li data-key="dashboard">
                    <a href="/mp-admin" title="Dashboard" class="text-truncate"><i class="fa fa-home"></i> Dashboard</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="javascript:void(0);" title="{!! trans('ECommercePanel::menu_titles.price') !!}"
                       class="text-truncate"><i
                            class="fa fa-sitemap"></i>{!! trans('ECommercePanel::menu_titles.price') !!}</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-content">
        @include('MPCorePanel::inc.errors')
        <div class="row">
            <div class=" col-sm-6 float-left">
                <div class="title"> {!! trans('ECommercePanel::menu_titles.price') !!}</div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="table-field mt-5 all-results">

            <table>
                <thead>
                <tr>
                    <th>Sayfa Yapıları</th>
                    <th>Ülke Grupları</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($sitemaps as $key=>$sitemap)
                        <tr>
                            <td>{{$sitemap}}</td>
                            <td>
                                @foreach($cg as $id=>$country)
                                    <a href="{!! route('Price.show',['id'=>$key,'cg'=>$id]) !!}" class="btn btn-primary">{{$country}}</a>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>


        </div>
    </div>
@endsection
