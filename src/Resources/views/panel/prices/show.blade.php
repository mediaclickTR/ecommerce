@extends('MPCorePanel::inc.app')

@section("content")
    <style type="text/css">
        .breadcrumb ul > li > a {
            max-width: 300px;
        }
        input.form-control:focus{
            border-color: #ced4da;
            box-shadow:none ;
        }
    </style>
    <div class="breadcrumb">
        <div class="float-left">
            <ul>
                <li data-key="dashboard">
                    <a href="/mp-admin" title="Dashboard" class="text-truncate"><i class="fa fa-home"></i> Dashboard</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="{!! route('Price.index') !!}" title="{!! trans('ECommercePanel::menu_titles.price') !!}"
                       class="text-truncate"><i
                                class="fa fa-sitemap"></i>{!! trans('ECommercePanel::menu_titles.price') !!}</a>
                </li>
                <li data-key="sitemap_main_edit">
                    <a href="javascript:void(0);"
                       title=" {!! $sitemap->detail->name !!}"
                       class="text-truncate"><i
                                class="fa fa-sitemap"></i>{!! $sitemap->detail->name  !!}</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="page-content">
        <div class="pagination_list">
            {{ isset($_GET) ? $pages->appends($_GET)->links() : $pages->links() }}
        </div>
        <div class="float-left">
            <div class="title"> {!! $country !!}</div>
        </div>

        <div class="clearfix"></div>
        <div class="float-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#exportModal">Export<i class="fas fa-file-export"></i></button>
            <button class="btn btn-primary" data-toggle="modal" data-target="#importModal">Import<i class="fas fa-file-import"></i></button>
        </div>

        <div class="search_content">
            <form action="?">
                <h5>Hızlı Arama</h5>
                <div class="form-group row">
                    <div class="col-lg-3">
                        <input name="key" placeholder="Anahtar Kelime" class="form-control" value="{!! isset($_GET['key']) && !empty($_GET['key']) ? $_GET['key']:'' !!}"/>
                    </div>
                    <div class="col-lg-3">
                        <select name="status" class="form-control">
                            <option value="">Tümü</option>
                            <option value="1" {!! isset($_GET['status']) && $_GET['status'] == 1 ? 'selected':'' !!}>Aktif</option>
                            <option value="2" {!! isset($_GET['status']) && $_GET['status'] == 2 ? 'selected':'' !!}>Pasif</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <button type="submit" class="btn btn-primary">Ara</button>
                        <button type="button" class="btn btn-danger" onClick="window.location.href='{!! \Request::url() !!}'">Temizle</button>
                    </div>
                </div>
            </form>
        </div>
        <div id="app">
            <table class="table table-hover">
                <tr align="center">
                    <th> Ürün Adı</th>
                    <th> Durumu</th>
                    <th> Stok</th>
                    <th> Ürün Kodu</th>
                    <th> Vergi Oranı</th>
                    <th> Liste Fiyatı</th>
                    <th> Satış Fiyatı</th>
                    <th> Taksit sayısı</th>
                    <th> Kur</th>
                    <th> Varvasyon</th>
                </tr>
                <tr v-for="(product,key) in list">
                    <td> @{{ product.name  }}</td>
                    <td v-if="product.status == 1"><span class="text-success">Aktif</span></td>
                    <td v-if="product.status != 1"><span class="text-danger">Pasif</span></td>
                    <td><input :id="'stock'+key" type="number" class="form-control" v-model="product.ecommerce.stock" :value="product.ecommerce.stock" @change="update(product.id,'stock',key)"></td>
                    <td><input :id="'code'+key" type="text" class="form-control" v-model="product.ecommerce.code" :value="product.ecommerce.code" @change="update(product.id,'code',key)"></td>
                    <td><input :id="'tax'+key" type="number" step="0.01" class="form-control" v-model="product.ecommerce.tax" :value="product.ecommerce.tax" @change="update(product.id,'tax',key)"></td>
                    <td><input :id="'price'+key" type="number" step="0.01" class="form-control" v-model="product.ecommerce.price" :value="product.ecommerce.price" @change="update(product.id,'price',key)"></td>
                    <td><input :id="'sale_price'+key" type="number" step="0.01" class="form-control" v-model="product.ecommerce.sale_price" :value="product.ecommerce.sale_price" @change="update(product.id,'sale_price',key)"></td>
                    <td><input :id="'installment'+key" type="number" class="form-control" v-model="product.ecommerce.installment" :value="product.ecommerce.installment" @change="update(product.id,'installment',key)"></td>
                    <td><select :id="'currency_id'+key" class="form-control" v-model="product.ecommerce.currency_id" @change="update(product.id,'currency_id',key)">
                            <option v-for="(currency,id) in currencies" :value="id">@{{ currency }}</option>
                        </select></td>
                    <td>
                        <button class="btn btn-primary varvasyonModal" :data-id="product.ecommerce.id" :data-name="product.name"><i class="fas fa-plus"></i></button>
                    </td>
                </tr>
            </table>

        </div>

    </div>

    <div class="modal fade" id="varvasyonModal" tabindex="-1" role="dialog" aria-labelledby="varvasyonModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="varvasyonModalLabel"><span class="modalHeaderText"></span> Ürün Varvasyonları</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addVariation"  method="post">
                        @csrf
                        <input type="hidden" name="product_id">
                        <table>
                            <thead>
                            <tr>
                                <th>Varvasyon Adı</th>
                                <th>Stok</th>
                                <th>Ürün Kodu</th>
                                <th>Liste Fiyatı</th>
                                <th>Satış Fiyatı</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <input type="text" class="form-control form-control-sm" name="variation_name"/>
                                </td>
                                <td>
                                    <input type="number" class="form-control form-control-sm" name="stock"/>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-control-sm" name="code"/>
                                </td>
                                <td>
                                    <input type="number" class="form-control form-control-sm" name="price"/>
                                </td>
                                <td>
                                    <input type="number" class="form-control form-control-sm" name="sale_price"/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="p-2">
                            <button type="submit" class="btn btn-secondary float-right mb-3 mt-3">Ekle</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exportModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excel Export</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{!! route("Price.export") !!}" method="post">
                        @csrf
                        <input type="hidden" name="cg" value="{{ $cg }}">
                        <input type="hidden" name="sitemap_id" value="{{ $sitemap->id }}">
                        <div class="form-group mt-3">
                            <select name="category_id" class="form-control">
                                <option value="">Kategori Seçiniz</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ strip_tags($category->detail->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="p-2">
                            <button type="submit" class="btn btn-secondary float-right mb-3 mt-3">Export</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excel Import</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{!! route("Price.import") !!}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group mt-3">
                            <input type="file" class="form-control-file" name="excel">
                        </div>
                        <div class="p-2">
                            <button type="submit" class="btn btn-secondary float-right mb-3 mt-3">{{ __('MPCorePanel::general.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script>
        window._token = '{!! csrf_token() !!}';
        window.cg = '{!! $cg !!}';
    </script>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                products: @json($products),
                currencies: @json($currencies),
                list : {},
                search:null
            },
            mounted(){
                this.list = this.products
            },
            methods:{
                update(id,name,key){
                    window.axios.post('/mp-admin/Price/update/'+id,{
                        _token:window._token,
                        id:id,
                        name: name,
                        cg:window.cg,
                        value: this.list[key].ecommerce[name]
                    }).then(function(){
                        $('#'+name+key).attr('style','border:1px solid green');
                        setTimeout(function(){
                            $('#'+name+key).removeAttr('style')
                        },2000)
                    })
                },
                searchProducts(){
                    this.list = this.products.filter(res => {
                        return res.name.toLowerCase().includes(this.search.toLowerCase())
                    });
                }
            }
        });
        var getVariations = function(product_id){
            $('table.variationListTable').remove();
            $.ajax({
                url:'{!! route("Price.getvariations") !!}',
                type:"GET",
                data:{
                    id:product_id
                },
                dataType:"json",
                success:function(response){
                    if( response.status == 1 ){
                        $('#varvasyonModal .modal-body').prepend(response.render);
                    }
                }
            });
        }
        $(document).on('click', '.varvasyonModal', function(){
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            $('.modalHeaderText').text(name);
            $('input[name="product_id"]').val(id);
            getVariations(id);
            $('#varvasyonModal').modal('show');
        });
        $('#addVariation').on('submit', function(event){
            event.preventDefault();
            var id = $(this).find('input[name="product_id"]').val();
            var variation_name = $(this).find('input[name="variation_name"]').val();
            var stock = $(this).find('input[name="stock"]').val();
            var code = $(this).find('input[name="code"]').val();
            var price = $(this).find('input[name="price"]').val();
            var sale_price = $(this).find('input[name="sale_price"]').val();
            if( id === "" || variation_name === "" || stock === "" || code === "" || price === "" || sale_price === "" ){
                alert('Lütfen tüm alanları doldurunuz.');
                return false;
            }
            $.ajax({
                url:'{!! route("Price.addvariation") !!}',
                type:"POST",
                data:{
                    id:id,
                    variation_name:variation_name,
                    stock:stock,
                    code:code,
                    price:price,
                    sale_price:sale_price,
                    _token:$('input[name="_token"]').val()
                },
                dataType:"json",
                success:function(response){
                    if( response.status == 1 ){
                        getVariations(id);
                        $('#addVariation input[type="text"], #addVariation input[type="number"]').each(function(){
                            $(this).val("");
                        });
                    }
                }
            });
            return false;
        });
        $(document).on('click', '.editVariation', function(event){
            event.preventDefault();
            var id = $(this).attr('data-id');
            var variation_name = $(this).closest('tr').find('input[name="variation_name"]').val();
            var stock = $(this).closest('tr').find('input[name="stock"]').val();
            var code = $(this).closest('tr').find('input[name="code"]').val();
            var price = $(this).closest('tr').find('input[name="price"]').val();
            var sale_price = $(this).closest('tr').find('input[name="sale_price"]').val();
            if( id === "" || variation_name === "" || stock === "" || code === "" || price === "" || sale_price === "" ){
                alert('Lütfen tüm alanları doldurunuz.');
                return false;
            }
            $.ajax({
                url:'{!! route("Price.editVariation") !!}',
                type:"POST",
                data:{
                    id:id,
                    variation_name:variation_name,
                    stock:stock,
                    code:code,
                    price:price,
                    sale_price:sale_price,
                    _token:$('input[name="_token"]').val()
                },
                dataType:"json",
                success:function(response){
                    if( response.status == 1 ){
                        $('#varvasyonModal .modal-body').prepend('<div class="alert alert-success">Varvasyon başarılı şekilde güncellendi.</div>');
                        setTimeout(function(){
                            $('#varvasyonModal .modal-body .alert').remove();
                        },3000);
                    }
                }
            });
            return false;
        });
        $(document).on('click', '.delVariation', function(){
            var id = $(this).attr('data-id');
            $.ajax({
                url:'{!! route("Price.delvariations") !!}',
                type:"GET",
                data:{
                    id:id
                },
                dataType:"json",
                success:function(response){
                    if( response.status == 1 ){
                        getVariations(response.parent_id);
                    }
                }
            });
        });
    </script>
@endpush
