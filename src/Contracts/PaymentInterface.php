<?php

namespace Mediapress\ECommerce\Contracts;

use Mediapress\ECommerce\Foundation\Order;

interface PaymentInterface
{

    public function __construct(Order $order);

    public function makePayment();
}
