<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Mediapress\ECommerce\Foundation\Banks\Iyzico;

class UpdatePaymentTypesTable extends Migration
{

    public function up()
    {

        if (Schema::hasTable('payment_types')) {
            Schema::table('payment_types', function (Blueprint $table) {
                $table->string('provider')->nullable()->after('bank');
                    });
        }
    }

    public function down()
    {
        if (Schema::hasTable('payment_types')) {
            Schema::table('payment_types', function (Blueprint $table) {
                $table->dropColumn('provider');
              });
        }
    }
}
