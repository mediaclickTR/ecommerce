<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration
{

    const ORDERS = 'orders';

    public function up()
    {
        if (!Schema::hasTable(self::ORDERS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create(self::ORDERS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('order_number');
                $table->tinyInteger('status');
                $table->unsignedInteger('user_id')->nullable();
                $table->unsignedInteger('payment_type_id')->nullable();
                $table->unsignedInteger('payment_card_id')->nullable();
                $table->unsignedInteger('billing_address_id')->nullable();
                $table->unsignedInteger('shipping_address_id')->nullable();
                $table->text('billing_address')->nullable();
                $table->text('shipping_address')->nullable();
                $table->string('currency');
                $table->double('sub_total', 12, 4)->default(0.00);
                $table->double('tax', 12, 4)->default(0.00);
                $table->double('total', 12, 4)->default(0.00);
                $table->double('campaign_discount', 12, 4)->nullable();
                $table->double('grand_total', 12, 4)->default(0.00);
                $table->timestamps();
                $table->softDeletes();

            });
        }
    }

    public function down()
    {
        Schema::dropIfExists(self::ORDERS);
    }
}
