<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartsTable extends Migration
{

    const CARTS = 'carts';

    public function up()
    {

        if (!Schema::hasTable(self::CARTS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create(self::CARTS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('order_id')->nullable();
                $table->unsignedInteger('user_id')->nullable();
                $table->string('session_id')->nullable();
                $table->unsignedInteger('quantity')->default(0);
                $table->double('sub_total', 12, 4)->default(0.00);
                $table->double('tax', 12, 4)->default(0.00);
                $table->double('total', 12, 4)->default(0.00);
                $table->integer('campaign_discount')->nullable();
                $table->integer('coupon_id')->nullable();
                $table->double('grand_total', 12, 4)->default(0.00);
                $table->string('currency');
                $table->timestamps();
                $table->softDeletes();

            });
        }
    }

    public function down()
    {
        Schema::dropIfExists(self::CARTS);
    }
}
