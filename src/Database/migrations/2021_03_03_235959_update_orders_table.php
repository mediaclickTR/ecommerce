<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateOrdersTable extends Migration
{

    public function up()
    {

        if (Schema::hasTable('orders')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->integer('order_status')->default(0)->after('status');
                $table->integer('installment')->default(1)->after('currency');
                $table->text('response')->default(null)->nullable()->after('grand_total');
                $table->text('info')->default(null)->nullable()->after('response');
                $table->text('agreement')->default(null)->nullable()->after('info');
                $table->decimal('payed_total',12,4)->default(null)->nullable()->after('grand_total');
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('orders')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('order_status');
                $table->dropColumn('info');
                $table->dropColumn('agreement');
                $table->dropColumn('installment');
                $table->dropColumn('response');
                $table->dropColumn('payed_total');
            });
        }
    }
}
