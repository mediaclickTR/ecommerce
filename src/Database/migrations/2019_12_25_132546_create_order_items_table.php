<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemsTable extends Migration
{

    const ORDER_ITEMS = 'order_items';

    public function up()
    {

        if (!Schema::hasTable(self::ORDER_ITEMS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create(self::ORDER_ITEMS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('order_id');
                $table->string('order_number');
                $table->tinyInteger('status');
                $table->enum('virtual_item', ["0", "1"])->default("0");
                $table->string('model_type')->nullable();
                $table->integer('model_id')->nullable();
                $table->string('model_price_key')->nullable();
                $table->string('currency');
                $table->text('information')->nullable();
                $table->double('quantity', 12, 4)->default(0.00);
                $table->double('sub_total', 12, 4)->default(0.00);
                $table->double('tax', 12, 4)->default(0.00);
                $table->double('total', 12, 4)->default(0.00);
                $table->double('grand_total', 12, 4)->default(0.00);
                $table->double('unit_price', 12, 4)->default(0.00);
                $table->double('unit_tax', 12, 4)->default(0.00);
                $table->string('base_currency');
                $table->double('base_unit_price', 12, 4)->default(0.00);
                $table->double('base_unit_tax', 12, 4)->default(0.00);
                $table->double('tax_rate', 6, 3)->default(0.00);
                $table->enum('tax_include', ["0", "1"])->default("0");
                $table->double('campaign_discount', 12, 4)->default(0.00);
                $table->string('custom_name')->nullable();
                $table->string('tag')->nullable();
                $table->text('criterias')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists(self::ORDER_ITEMS);
    }
}
