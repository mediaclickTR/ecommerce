<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentAddressesTable extends Migration
{

    const PAYMENT_ADDRESS = 'payment_addresses';

    public function up()
    {

        if (!Schema::hasTable(self::PAYMENT_ADDRESS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create(self::PAYMENT_ADDRESS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id');
                $table->string('contact_name')->nullable();
                $table->string('contact_email')->nullable();
                $table->string('contact_phone')->nullable();
                $table->string('address_title')->nullable();
                $table->text('address');
                $table->string('region')->nullable();
                $table->string('city')->nullable();
                $table->string('country')->nullable();
                $table->bigInteger('zip_code')->nullable();


                $table->enum('type', ['individual', 'corporate'])->default('individual');
                $table->string('corporate_name')->nullable();
                $table->bigInteger('corporate_tax_number')->nullable();
                $table->string('corporate_tax_office')->nullable();
                $table->bigInteger('id_number')->nullable();
                $table->string('name_surname')->nullable();
                $table->enum('ebill_payer', ['0', '1'])->default('0');


                $table->string('cvar_1')->nullable();
                $table->string('cvar_2')->nullable();
                $table->text('ctex_1')->nullable();
                $table->text('ctex_2')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists(self::PAYMENT_ADDRESS);
    }
}
