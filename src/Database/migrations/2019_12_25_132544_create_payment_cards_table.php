<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentCardsTable extends Migration
{

    const PAYMENT_CARDS = 'payment_cards';

    public function up()
    {

        if (!Schema::hasTable(self::PAYMENT_CARDS)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create(self::PAYMENT_CARDS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id');
                $table->string('card_holder_name')->nullable();
                $table->string('card_number');
                $table->integer('expire_month');
                $table->integer('expire_year');

                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists(self::PAYMENT_CARDS);
    }
}
