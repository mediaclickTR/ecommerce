<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentTypesTable extends Migration
{

    const PAYMENT_TYPE = 'payment_types';

    public function up()
    {

        if (!Schema::hasTable(self::PAYMENT_TYPE)) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create(self::PAYMENT_TYPE, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('type');
                $table->string('bank');
                $table->text('detail')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists(self::PAYMENT_TYPE);
    }
}
