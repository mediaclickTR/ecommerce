<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

    public function up()
    {

        if (!Schema::hasTable('products')) {
            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });
            $schema->create('products', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('page_id');
                $table->integer('country_group_id');
                $table->integer('stock');
                $table->string('code');
                $table->decimal('tax');
                $table->decimal('price');
                $table->decimal('sale_price');
                $table->integer('installment');
                $table->integer('currency_id');
                $table->string('variation_name', 500);
                $table->integer('parent_id')->default('0');
                $table->timestamps();
                $table->softDeletes();

            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
