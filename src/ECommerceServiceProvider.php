<?php

namespace Mediapress\ECommerce;

use Illuminate\Foundation\AliasLoader;
//use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Mediapress\ECommerce\Facades\ECommerce;
use Mediapress\Modules\Module as ServiceProvider;

class ECommerceServiceProvider extends ServiceProvider
{
    public $namespace = 'Mediapress\ECommerce';
    protected $module_name = "ECommerce";
    public const RESOURCES = 'Resources';
    public const VIEWS = 'views';


    public function boot()
    {
        $this->map();
        $this->loadViewsFrom(__DIR__.DIRECTORY_SEPARATOR.self::RESOURCES.DIRECTORY_SEPARATOR.'views/panel', 'ECommerceView');
        $this->publishes([__DIR__.'/Config' => config_path()], "ECommercePublishes");
        $this->loadTranslationsFrom(__DIR__. DIRECTORY_SEPARATOR . 'Resources/lang', 'ECommercePanel');

        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'Http' . DIRECTORY_SEPARATOR . "helpers.php")) {
            include_once __DIR__ . DIRECTORY_SEPARATOR . 'Http' . DIRECTORY_SEPARATOR . "helpers.php";
        }
        $this->loadMigrationsFrom(__DIR__ . '/Database/migrations');
    }

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('ECommerce', \Mediapress\ECommerce\Facades\ECommerce::class);
        app()->bind('ECommerce', function () {
            return new \Mediapress\ECommerce\ECommerce;
        });
//        $this->mergeConfig(__DIR__ . '/Config/ecommerce_module_actions.php', 'ecommerce_module_actions');
        $this->mergeConfigFrom(__DIR__ . DIRECTORY_SEPARATOR . "Config" . DIRECTORY_SEPARATOR . "ecommerce.php", "ecommerce");
    }

    public function map()
    {
        $this->mapPanelRoutes();
    }

    protected function mapPanelRoutes()
    {
        $routes_file =  __DIR__ . DIRECTORY_SEPARATOR . 'Routes' . DIRECTORY_SEPARATOR . 'panel.php';
        Route::middleware('web')
            ->namespace( $this->namespace . '\Http\Controllers\Panel')
            ->prefix( 'mp-admin')
            ->group($routes_file);

    }
}
