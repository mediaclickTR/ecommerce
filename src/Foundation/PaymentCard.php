<?php


namespace Mediapress\ECommerce\Foundation;


class PaymentCard
{

    private $id;
    private $holder_name;
    private $card_number;
    private $expire_month;
    private $expire_year;
    private $ccv;

    /**
     * @return $this
     */
    public function save() : self
    {
        $temp = $this->card_number;
        $this->card_number = substr($temp, 0, 6) . str_repeat("*", 6) . substr($temp, -4);

        $payment_card = \Mediapress\ECommerce\Models\PaymentCard::updateOrCreate(
            [
                "card_holder_name" => $this->holder_name,
                "card_number" => $this->card_number,
                "expire_month" => $this->expire_month,
                "expire_year" => $this->expire_year,
            ]
        );
        $this->id = $payment_card->id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getHolderName(): string
    {
        return $this->holder_name;
    }

    /**
     * @param string $holder_name
     * @return $this
     */
    public function setHolderName(string $holder_name): self
    {
        $this->holder_name = $holder_name;

        return $this;
    }


    /**
     * @return string
     */
    public function getCardNumber(): string
    {
        return $this->card_number;
    }

    /**
     * @param $card_number
     * @return $this
     */
    public function setCardNumber($card_number): self
    {
        $this->card_number = $card_number;

        return $this;
    }


    /**
     * @return int
     */
    public function getExpireMonth(): int
    {
        return $this->expire_month;
    }


    /**
     * @param $expire_month
     * @return $this
     */
    public function setExpireMonth($expire_month): self
    {
        $this->expire_month = $expire_month;

        return $this;
    }


    /**
     * @return int
     */
    public function getExpireYear(): int
    {
        return $this->expire_year;
    }


    /**
     * @param $expire_year
     * @return $this
     */
    public function setExpireYear($expire_year): self
    {
        $this->expire_year = $expire_year;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcv(): ?string
    {
        return $this->ccv;
    }

    /**
     * @param mixed $ccv
     */
    public function setCcv($ccv): self
    {
        $this->ccv = $ccv;

        return $this;
    }


}
