<?php


namespace Mediapress\ECommerce\Foundation\Banks;

use Illuminate\Http\Request;
use Iyzipay\Model\InstallmentInfo;
use Iyzipay\Model\Payment;
use Iyzipay\Model\ThreedsInitialize;
use Iyzipay\Request\RetrieveInstallmentInfoRequest;
use Mediapress\ECommerce\Contracts\PaymentInterface;
use Mediapress\ECommerce\Foundation\Order;
use Mediapress\ECommerce\Models\PaymentType;

class Iyzico extends BaseBank implements PaymentInterface
{


    /**
     * @var Order
     */
    private $order;
    private $paidPrice;
    /**
     * @var array|object
     */
    private $installment;
    private \Iyzipay\Options $options;
    private \Iyzipay\Request\CreatePaymentRequest $request;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function makePayment()
    {
        $installment = $this->checkInstallment();
        $detail = $this->getDetail();
        $options = $this->setOptions($detail);
        $request = $this->createPaymentRequest();
        $request = $this->setPaymentCard($request);
        $request = $this->setBuyer($request);
        $request = $this->setShippingAddress($request);
        $request = $this->setBillingAddress($request);
        $request = $this->setBasketItems($request);
        if ($installment->force3D) {
            session(['order.' . $this->order->getOrderNumber() => $this->order]);
            $request->setCallbackUrl(url('ecommerce-payment') . '?referer=' . urlencode(request()->header('referer')));

            $payment = ThreedsInitialize::create($request, $options);

        } else {
            $payment = Payment::create($request, $options);
        }
        $process = $this->createResponse($payment, $installment->force3D);

        if (is_array($process) && $process['status'] == true) {
            $this->order->makeOrderSuccessed($process['response']->getPaidPrice(), json_decode($process['response']->getRawResult(),1));
        }
        return $process;

    }

    private function getDetail()
    {
        $paymentType = $this->order->getPaymentType();
        return $paymentType['detail'];
    }

    private function setOptions($detail)
    {
        $options = new \Iyzipay\Options();
        $options->setApiKey($detail['api_key']);
        $options->setSecretKey($detail['secret_key']);
        $options->setBaseUrl($detail['api_url']);

        return $options;
    }

    private function createPaymentRequest()
    {

        $request = new \Iyzipay\Request\CreatePaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId($this->order->getOrderNumber());
        $request->setCurrency($this->order->getCurrency());
        $request->setInstallment($this->order->getInstallment());
        $request->setBasketId($this->order->getOrderNumber());
        $request->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);

        return $request;
    }

    private function setPaymentCard(\Iyzipay\Request\CreatePaymentRequest $request)
    {

        $creditCard = $this->order->getPaymentCard();
        $paymentCard = new \Iyzipay\Model\PaymentCard();
        $paymentCard->setCardHolderName($creditCard->getHolderName());
        $paymentCard->setCardNumber($creditCard->getCardNumber());
        $paymentCard->setExpireMonth($creditCard->getExpireMonth());
        $paymentCard->setExpireYear($creditCard->getExpireYear());
        $paymentCard->setCvc($creditCard->getCcv());
        $paymentCard->setRegisterCard(0);

        $request->setPaymentCard($paymentCard);

        return $request;
    }

    private function setBuyer(\Iyzipay\Request\CreatePaymentRequest $request)
    {
        $user = auth()->user();

        $billing = $this->order->getBillingAddress();

        $name = $this->split_name($user->name);

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId($user->id);
        $buyer->setName($name[0]);
        $buyer->setSurname($name[1]);
        $buyer->setGsmNumber($user->phone);
        $buyer->setEmail($user->email);
        $buyer->setIdentityNumber(($billing->type == 'individual' ? $billing->id_number : $billing->corporate_tax_number) ?: 22222222220);
        $buyer->setLastLoginDate(now()->format('Y-m-d H:i:s'));
        $buyer->setRegistrationDate($user->created_at->format('Y-m-d H:i:s'));
        $buyer->setRegistrationAddress($billing->address);
        $buyer->setIp(request()->ip);
        $buyer->setCity($billing->city ?: 'Istanbul');
        $buyer->setCountry($billing->country ?: "Turkey");
        $buyer->setZipCode($billing->zip_code ?: '34000');

        $request->setBuyer($buyer);

        return $request;
    }

    private function setShippingAddress(\Iyzipay\Request\CreatePaymentRequest $request)
    {
        $user = auth()->user();
        $address = $this->order->getShippingAddress();
        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName($user->name);
        $shippingAddress->setCity($address->city ?: 'Istanbul');
        $shippingAddress->setCountry($address->country ?: "Turkey");
        $shippingAddress->setAddress($address->address);
        $shippingAddress->setZipCode($address->zip_code ?: '34000');
        $request->setShippingAddress($shippingAddress);

        return $request;
    }

    private function setBillingAddress(\Iyzipay\Request\CreatePaymentRequest $request)
    {

        $user = auth()->user();

        $address = $this->order->getBillingAddress();
        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName($user->name);
        $billingAddress->setCity($address->city ?: 'Istanbul');
        $billingAddress->setCountry($address->country ?: "Turkey");
        $billingAddress->setAddress($address->address);
        $billingAddress->setZipCode($address->zip_code ?: '34000');
        $request->setBillingAddress($billingAddress);

        return $request;
    }

    private function setBasketItems(\Iyzipay\Request\CreatePaymentRequest $request)
    {
        $basketItems = array();
        $total = 0;

        foreach ($this->order->getItems() as $item) {
            if ($item->getGrandTotal() < 0.01) {
                continue;
            }

            $basketItem = new \Iyzipay\Model\BasketItem();
            $basketItem->setId($item->getModelId());
            $basketItem->setName($item->getCustomName() ?: randomString(6));
            $basketItem->setCategory1($item->getModelType());
            $basketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
            $price = round($item->getGrandTotal(), 2);
            $basketItem->setPrice($price);

            $total += $price;

            $basketItems[] = $basketItem;

        }

        $request->setPrice($total);
        $installment = $this->checkInstallment();
        if ($installment->installments) {
            foreach ($installment->installments as $install) {
                if ($install['installmentNumber'] == $this->order->getInstallment()) {
                    $this->paidPrice = $install['totalPrice'];
                }
            }
        }
        if ($this->paidPrice) {
            $request->setPaidPrice($this->paidPrice);
        } else {
            $request->setPaidPrice($total);
        }

        $request->setBasketItems($basketItems);

        return $request;
    }

    private function createResponse($payment, $force3D)
    {
        if ($force3D) {

            if ($payment->getStatus() != "success") {
                $response = [
                    "status" => false,
                    "response" => $payment,
                    "messages" => $payment->getErrorMessage()
                ];
            }

            if ($payment->getHtmlContent()) {
                return response($payment->getHtmlContent());
            }
        } else {
            $response = [
                'status' => $payment->getStatus() == 'success',
                'response' => $payment
            ];

            if ($payment->getStatus() != 'success') {
                $response['messages'] = $payment->getErrorMessage();
            }
        }

        return $response;
    }

    private function checkInstallment()
    {
        $card_no = $this->order->getPaymentCard()->getCardNumber();
        return $this->getInstallmentList(substr($card_no, 0, 6));

    }

    private function getInstallmentList($bin)
    {
        $installment_request = new RetrieveInstallmentInfoRequest();
        $installment_request->setLocale(\Iyzipay\Model\Locale::TR);
        $installment_request->setConversationId($this->order->getOrderNumber());
        $installment_request->setBinNumber($bin);
        $installment_request->setPrice(round($this->order->getGrandTotal()));

        $installment_info = InstallmentInfo::retrieve($installment_request, $this->setOptions($this->getDetail()));

        if ($installment_info->getStatus() == "success") {

            $installment_prices = head($installment_info->getInstallmentDetails())->getInstallmentPrices();
            $details = [];

            foreach ($installment_prices as $price) {
                $details[] = [
                    "installmentPrice" => $price->getInstallmentPrice(),
                    "totalPrice" => $price->getTotalPrice(),
                    "installmentNumber" => $price->getInstallmentNumber(),
                ];
            }
            $force_3d = ["force3D" => head($installment_info->getInstallmentDetails())->getForce3ds()];

            $array = array_merge(['installments' => $details], $force_3d);

            return (object)$array;

        } else {
            return ["error" => $installment_info->getErrorMessage()];
        }
    }

    public static function check3DPayment(Order $order, Request $request)
    {

        $threed_callback = new \Iyzipay\Request\CreateThreedsPaymentRequest();
        $threed_callback->setLocale(\Iyzipay\Model\Locale::TR);
        $threed_callback->setConversationId($request->conversationId);
        $threed_callback->setPaymentId($request->paymentId);
        $threed_callback->setConversationData($request->conversationData);

        $iyzico = new Iyzico($order);

        return \Iyzipay\Model\ThreedsPayment::create(
            $threed_callback,
            $iyzico->setOptions($iyzico->getDetail())
        );

    }

    public function getInstallmentInfo($bin)
    {
        $bin = str_replace(' ', '', $bin);
        $bin = substr($bin, 0, 6);
        return $this->getCardDetails($bin);
    }

    public function getCardDetails($bin)
    {

        $iyzico = new RetrieveInstallmentInfoRequest();
        $iyzico->setLocale(\Iyzipay\Model\Locale::TR);
        $iyzico->setConversationId($this->order->getOrderNumber());
        $iyzico->setBinNumber($bin);
        $iyzico->setCurrency($this->order->getCurrency());
        $iyzico->setPrice($this->order->getGrandTotal());
        $detail = $this->getDetail();

        $options = $this->setOptions($detail);

        $installmentInfo = \Iyzipay\Model\InstallmentInfo::retrieve($iyzico, $options);
        return $installmentInfo;
    }
}
