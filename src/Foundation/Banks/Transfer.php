<?php


namespace Mediapress\ECommerce\Foundation\Banks;


use Mediapress\ECommerce\Contracts\PaymentInterface;
use Mediapress\ECommerce\Foundation\Order;


class Transfer extends BaseBank implements PaymentInterface
{

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function makePayment()
    {
        // TODO: Implement makePayment() method.
        $this->order->makeOrderSuccessed($this->order->getGrandTotal(),[],0);

        return $this->order;
    }
}
