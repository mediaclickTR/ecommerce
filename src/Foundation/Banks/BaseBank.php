<?php


namespace Mediapress\ECommerce\Foundation\Banks;


abstract class BaseBank{


    protected function split_name($name) {

        $name = trim($name);
        $name = explode(' ', $name);

        $last_name = array_pop($name);
        $first_name = implode(' ',$name);

        if( empty($first_name) ) {
            $first_name = $last_name;
        }

        return array($first_name, $last_name);
    }
}
