<?php


namespace Mediapress\ECommerce\Foundation\Banks;

use Mediapress\ECommerce\Contracts\PaymentInterface;
use Mediapress\ECommerce\Foundation\Order;

class TurkiyeFinans extends BaseBank implements PaymentInterface
{
    /**
     * Test Card => 5377195377190410, 4799174799173828
     * @var Order
     */
    private $order;

    protected $currencies = [
        'TRY' => 949,
        'USD' => 840,
        'EUR' => 978,
        'GBP' => 826,
        'JPY' => 392,
        'RUB' => 643,
    ];

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function makePayment()
    {
        $detail = $this->getDetail();
        $options = $this->setOptions($detail);
        $this->createPaymentRequest($options);
    }

    private function getDetail()
    {
        $paymentType = $this->order->getPaymentType();
        return $paymentType['detail'];
    }

    private function setOptions($detail)
    {
        $options = new \stdClass();
        $options->client_id = $detail['client_id'];
        $options->store_key = $detail['store_key'];
        $options->api_url = $detail['api_url'];

        return $options;
    }

    private function createPaymentRequest($option)
    {
        $random = uniqid('rnd');
        $cardNumber = str_replace(" ", "", $this->order->getPaymentCard()->getCardNumber());
        $hash = $this->createHash($option, $random);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $option->api_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "clientid=" . $option->client_id .
            "&islemtipi=Auth" .
            "&amount=" . $this->order->getTotal() .
            "&oid=" . $this->order->getOrderNumber() .
            "&okUrl=" . route('payment.get')."?status=success" .
            "&failUrl=" . route('payment.get').'?status=fail' .
            "&rnd=" . $random .
            "&hash=" . $hash .
            "&storetype=3D" .
            "&lang=tr" .
            "&currency=" . $this->currencies[$this->order->getCurrency()] .
            "&pan=" . $cardNumber .
            "&cv2=" . $this->order->getPaymentCard()->getCcv() .
            "&Ecom_Payment_Card_ExpDate_Year=" . $this->order->getPaymentCard()->getExpireYear() .
            "&Ecom_Payment_Card_ExpDate_Month=" . $this->order->getPaymentCard()->getExpireMonth() .
            "&cardType=" . ($this->order->getCardType() == 'Visa' ? '1' : '2')
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        session(['order.'.$this->order->getOrderNumber() => $this->order]);
        session()->save();

        echo $response;
        exit();
    }

    private function createHash($option, $random)
    {
        $plaintext = $option->client_id .
            $this->order->getOrderNumber() .
            $this->order->getTotal() .
            route('payment.get')."?status=success" .
            route('payment.get').'?status=fail' .
            "Auth".
            $random .
            $option->store_key;

        return base64_encode(pack('H*', sha1($plaintext)));
    }

}
