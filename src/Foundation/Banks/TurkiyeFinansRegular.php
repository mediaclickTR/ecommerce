<?php


namespace Mediapress\ECommerce\Foundation\Banks;

use Mediapress\ECommerce\Contracts\PaymentInterface;
use Mediapress\ECommerce\Foundation\Order;
use SimpleXMLElement;
use function Couchbase\defaultDecoder;

class TurkiyeFinansRegular extends BaseBank implements PaymentInterface
{
    /**
     * Test Card => 5377195377190410, 4799174799173828
     * @var Order
     */
    private $order;

    protected $currencies = [
        'TRY' => 949,
        'USD' => 840,
        'EUR' => 978,
        'GBP' => 826,
        'JPY' => 392,
        'RUB' => 643,
    ];

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function makePayment()
    {
        $detail = $this->getDetail();
        $options = $this->setOptions($detail);
        $this->createPaymentRequest($options);
    }

    private function getDetail()
    {
        $paymentType = $this->order->getPaymentType();
        return $paymentType['detail'];
    }

    private function setOptions($detail)
    {
        $options = new \stdClass();
        $options->client_id = $detail['client_id'];
        $options->store_key = $detail['store_key'];
        $options->api_url = $detail['api_url'];
        $options->name = $detail['name'];
        $options->password = $detail['password'];

        return $options;
    }

    private function createPaymentRequest($option)
    {
        try{
            $cardNumber = str_replace(" ", "", $this->order->getPaymentCard()->getCardNumber());
            $info = json_decode($this->order->getInfo(), 1);

            $orderItems = $this->order->getItems();
            $projects = '';
            foreach ($orderItems as $order_item) {
                $itemId = rand(100, 100000);
                $projects .= "<OrderItem>" .
                    "<ItemNumber>" . $itemId . "</ItemNumber>" .
                    "<ProductCode>" . $order_item->getOrderNumber() . "</ProductCode>" .
                    "<Qty>" . $order_item->getQuantity() . "</Qty>" .
                    "<Desc>" . str_replace(['İ', 'Ş', 'Ç', 'Ö', 'Ü', 'Ğ', '!'], ['I', 'S', 'C', 'O', 'U', 'G', ''], $order_item->getCustomName()) . "</Desc>" .
                    "<Id>" . $itemId . "</Id>" .
                    "<Price>" . $order_item->getBaseUnitPrice() . "</Price>" .
                    "<Total>" . $order_item->getTotal() . "</Total>" .
                    "</OrderItem>";
            }

            $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <CC5Request>
                    <Name>' . $option->name . '</Name>
                    <Password>' . $option->password . '</Password>
                    <ClientId>' . $option->client_id . '</ClientId>
                    <OrderId>' . $this->order->getOrderNumber() . '</OrderId>
                    <Email>' . $info["email"] . '</Email>
                    <Type>Auth</Type>
                    <Number>' . $cardNumber . '</Number>
                    <Expires>' . $this->order->getPaymentCard()->getExpireMonth() . '/' . $this->order->getPaymentCard()->getExpireYear() . '</Expires>
                    <Cvv2Val>' . $this->order->getPaymentCard()->getCcv() . '</Cvv2Val>
                    <Total>' . $this->order->getTotal() . '</Total>
                    <Currency>' . $this->currencies[$this->order->getCurrency()] . '</Currency>
                    <PbOrder>
                        <OrderType>0</OrderType>
                        <TotalNumberPayments>' . $info["repeatCount"] . '</TotalNumberPayments>
                        <OrderFrequencyCycle>M</OrderFrequencyCycle>
                        <OrderFrequencyInterval>1</OrderFrequencyInterval>
                    </PbOrder>
                     <BillTo>
                        <Name>' . $info["name"] . ' ' . $info["surname"] . '</Name>
                        <Email>' . $info["email"] . '</Email>
                        <TelVoice>' . $info["phone"] . '</TelVoice>
                    </BillTo>
                    <ShipTo>
                        <Name></Name>
                    </ShipTo>
                    <OrderItemList>' .
                $projects .
                '</OrderItemList>
                </CC5Request>';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $option->api_url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control: no-cache",
                "Content-Type: application/xml"
            ));
            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode(json_encode(simplexml_load_string($response)), true);

            session(['order.'.$this->order->getOrderNumber() => $this->order]);
            session()->save();

            if ($response['Response'] == 'Approved') {
                $this->order->makeOrderSuccessed($this->order->getTotal(), $response);
            } else {
                $this->order->makeOrderFailed($response);
            }
        }catch (\Exception $e) {
            $this->order->makeOrderFailed($response);
        }

    }


}
