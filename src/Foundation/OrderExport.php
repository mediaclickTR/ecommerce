<?php

namespace Mediapress\ECommerce\Foundation;

use Maatwebsite\Excel\Concerns\FromArray;
use Mediapress\ECommerce\Models\Order;
use Mediapress\Modules\Content\Models\PageDetail;
use Carbon\Carbon;

class OrderExport implements FromArray
{

    public function array(): array
    {
        $headRow = getExcelTitleRow();
        $hold = [];
        $orders = Order::where('status', 5)->with('items')->orderByDesc('created_at')->get()->toArray();

        $i = 1;
        foreach ($orders as $order) {
            $items = '';
            $items_total = 0;
            $items_quantity = 0;
            foreach ($order['items'] as $item) {
                $items .= strip_tags($item['custom_name']).',';
                $items_total += $item['total'];
                $items_quantity += $item['quantity'];
            }

            $info = json_decode($order['info'], 1);
            $hold[] = [
                $headRow[0] => $i++,
                $headRow[1] => $order['order_number'],
                $headRow[2] => rtrim($items, ','),
                $headRow[3] => $order['payment_type_id'] == 1 ? 'Tek Seferlik' : 'Düzenli',
                $headRow[4] => $items_total. ' TL',
                $headRow[5] => $items_quantity,
                $headRow[6] => $info['name'],
                $headRow[7] => $info['surname'],
                $headRow[8] => $info['email'],
                $headRow[9] => $info['phone'],
                $headRow[10] => $info['identity'],
                $headRow[11] => $info['note'],
                $headRow[12] => Carbon::parse($order['created_at'])->format('d.m.Y'),
            ];
        }

        $hold = array_merge([$headRow], $hold);

        return $hold;
    }

}
