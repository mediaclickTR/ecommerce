<?php


namespace Mediapress\ECommerce\Foundation;

use Illuminate\Http\Response;
use Iyzipay\Model\Payment;
use Mediapress\ECommerce\Foundation\Banks\Iyzico;
use Mediapress\ECommerce\Models\PaymentAddress;
use Mediapress\ECommerce\Models\PaymentType;

class Order
{

    /*
    *   1     Ödeme Bekliyor
    *   2     İşlemde
    *   3     İptal
    *   4     Geçersiz
    *   5     Tamamlandı
    */

    private $id;
    private $order_number;
    private $status = 1;
    private $order_status = 0;
    private $user_id;
    private $payment_type_id;
    private $payment_card_id;
    private $billing_address_id;
    private $shipping_address_id;
    private $billing_address;
    private $shipping_address;
    private $currency;
    private $sub_total;
    private $tax;
    private $total;
    private $campaign_discount;
    private $grand_total;

    public $items = array();
    public $payment_card;
    public $payment_type;
    public $card_type;
    private $installment = 1;
    private $paymentStatus;
    private $response;
    private $paidPrice;
    private $info;
    private $agreement;

    public static $creditcardTypes = [
        [
            'Name' => 'American Express',
            'cardLength' => [15],
            'cardPrefix' => ['34', '37'],
        ], [
            'Name' => 'Maestro',
            'cardLength' => [12, 13, 14, 15, 16, 17, 18, 19],
            'cardPrefix' => ['5018', '5020', '5038', '6304', '6759', '6761', '6763'],
        ], [
            'Name' => 'Mastercard',
            'cardLength' => [16],
            'cardPrefix' => ['51', '52', '53', '54', '55'],
        ], [
            'Name' => 'Visa',
            'cardLength' => [13, 16],
            'cardPrefix' => ['4'],
        ], [
            'Name' => 'JCB',
            'cardLength' => [16],
            'cardPrefix' => ['3528', '3529', '353', '354', '355', '356', '357', '358'],
        ], [
            'Name' => 'Discover',
            'cardLength' => [16],
            'cardPrefix' => ['6011', '622126', '622127', '622128', '622129', '62213','62214', '62215', '62216', '62217', '62218', '62219','6222', '6223', '6224', '6225', '6226', '6227', '6228','62290', '62291', '622920', '622921', '622922', '622923','622924', '622925', '644', '645', '646', '647', '648','649', '65'],
        ], [
            'Name' => 'Solo',
            'cardLength' => [16, 18, 19],
            'cardPrefix' => ['6334', '6767'],
        ], [
            'Name' => 'Unionpay',
            'cardLength' => [16, 17, 18, 19],
            'cardPrefix' => ['622126', '622127', '622128', '622129', '62213', '62214','62215', '62216', '62217', '62218', '62219', '6222', '6223','6224', '6225', '6226', '6227', '6228', '62290', '62291','622920', '622921', '622922', '622923', '622924', '622925'],
        ], [
            'Name' => 'Diners Club',
            'cardLength' => [14],
            'cardPrefix' => ['300', '301', '302', '303', '304', '305', '36'],
        ], [
            'Name' => 'Diners Club US',
            'cardLength' => [16],
            'cardPrefix' => ['54', '55'],
        ], [
            'Name' => 'Diners Club Carte Blanche',
            'cardLength' => [14],
            'cardPrefix' => ['300', '305'],
        ], [
            'Name' => 'Laser',
            'cardLength' => [16, 17, 18, 19],
            'cardPrefix' => ['6304', '6706', '6771', '6709'],
        ],
    ];

    public function makePayment()
    {
        $process = $this->paymentProcess();

        if ($process instanceof Response) {
            return $process;
        }

        $this->paymentStatus = $process;

        return $this;
    }

    public function getInstallmentInfo($bin, $payment_type_id = 1)
    {
        if ($payment_type_id) {
            $this->setPaymentTypeId($payment_type_id);
        }
        $provider = $this->payment_type['provider'];
        $bank = new $provider($this);
        return $bank->getInstallmentInfo($bin);
    }

    public function makeOrderSuccessed($paidPrice = null, $response = [],$orderStatus = 1)
    {
        $this->status = 2;
        $this->order_status = $orderStatus;
        $this->paidPrice = $paidPrice;
        $this->response = $response;

        $this->save();

        session()->forget('mediapress.ecommerce.cart');
    }

    public function makeOrderFailed($response = [])
    {
        $this->status = 4;
        $this->response = $response;
        $this->save();
    }

    public function save()
    {
        $this->clearOld();
        $this->setAddresses();
        $this->savePaymentCard();
        $this->saveToDB();
        $this->saveItemToDB();
        if ($this->payment_card) {
            $this->payment_card->save();
        }
    }

    public function getItemByTag($tag)
    {
        foreach ($this->items as $item) {
            if ($item->getTag() == $tag) {
                return $item;
            }
        }
        return null;
    }

    protected function paymentProcess()
    {
        $this->status = 1;
        $provider = $this->payment_type['provider'];
        $bank = new $provider($this);
        $this->saveToDB();
        $this->saveItemToDB();
        return $bank->makePayment();
    }

    protected function setAddresses(): void
    {
        $billing_address = PaymentAddress::find($this->billing_address_id);
        $shipping_address = PaymentAddress::find($this->shipping_address_id);

        if ($billing_address) {
            $this->billing_address = $billing_address->toArray();
        }

        if ($shipping_address) {
            $this->shipping_address = $shipping_address->toArray();
        }

    }

    protected function saveToDB(): void
    {

        $order = \Mediapress\ECommerce\Models\Order::updateOrCreate(
            [
                "order_number" => $this->order_number,
                "user_id" => $this->user_id,
            ],
            [
                "payment_type_id" => $this->payment_type_id,
                "status" => $this->status,
                "order_status" => $this->order_status,
                "payment_card_id" => $this->payment_card_id,
                "billing_address_id" => $this->billing_address_id,
                "shipping_address_id" => $this->shipping_address_id,
                "billing_address" => $this->billing_address,
                "shipping_address" => $this->shipping_address,
                "currency" => $this->currency,
                "sub_total" => $this->sub_total,
                "tax" => $this->tax,
                "total" => $this->total,
                'installment' => $this->installment,
                "campaign_discount" => $this->campaign_discount,
                "grand_total" => $this->grand_total,
                "payed_total" => $this->paidPrice,
                'response' => $this->response,
                'info' => $this->info,
                'agreement' => $this->agreement
            ]
        );
        $this->id = $order->id;
    }

    protected function saveItemToDB(): void
    {

        $temp_items = [];
        /** @var OrderItem $item */
        foreach ($this->getItems() as $item) {
            $item->setOrderId($this->id);
            $item->setOrderNumber($this->getOrderNumber());

            $item->saveToDB();

            $temp_items[$item->getId()] = $item;
        }
        $items = $temp_items;
    }

    public function addItems($items): self
    {
        $holder_item = [];
        foreach ($items as $item) {
            $order_item = new OrderItem($item);
            $order_item->setOrderNumber($this->order_number);

            $holder_item[] = $order_item;
        }

        $this->items = $holder_item;

        return $this;
    }

    public function getItems()
    {

        return $this->items;
    }

    public function setCreditCard(PaymentCard $card): self
    {
        $this->payment_card = $card;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOrderNumber(): string
    {
        return $this->order_number;
    }

    public function setOrderNumber($order_number): self
    {
        $this->order_number = $order_number;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOrderStatus()
    {
        return $this->order_status;
    }

    public function setOrderStatus($status)
    {
        $this->order_status = $status;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId($user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getPaymentTypeId(): ?int
    {
        return $this->payment_type_id;
    }

    public function setPaymentTypeId($payment_type_id): self
    {
        $this->payment_type_id = $payment_type_id;

        $payment_type = PaymentType::select('id', 'type', 'bank', 'detail', 'provider')->find($payment_type_id);
        if ($payment_type) {
            $this->payment_type = $payment_type->toArray();
        }

        return $this;
    }

    public function getPaymentCardId(): ?int
    {
        return $this->payment_card_id;
    }

    public function setPaymentCardId($payment_card_id): self
    {
        $this->payment_card_id = $payment_card_id;

        return $this;
    }

    public function getBillingAddressId(): ?int
    {
        return $this->billing_address_id;
    }

    public function setBillingAddressId($billing_address_id): self
    {
        $this->billing_address_id = $billing_address_id;

        return $this;
    }

    public function getShippingAddressId(): ?int
    {
        return $this->shipping_address_id;
    }

    public function setShippingAddressId($shipping_address_id): self
    {
        $this->shipping_address_id = $shipping_address_id;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency($currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getSubTotal(): ?float
    {
        return $this->sub_total;
    }

    public function setSubTotal($sub_total): self
    {
        $this->sub_total = $sub_total;

        return $this;
    }

    public function getTax(): ?float
    {
        return $this->tax;
    }

    public function setTax($tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getCampaignDiscount(): ?float
    {
        return $this->campaign_discount;
    }

    public function setCampaignDiscount($campaign_discount): self
    {
        $this->campaign_discount = $campaign_discount;

        return $this;
    }

    public function getGrandTotal(): ?float
    {
        return $this->grand_total;
    }

    public function setGrandTotal($grand_total): self
    {
        $this->grand_total = $grand_total;

        return $this;
    }

    public function getPaymentType()
    {
        return $this->payment_type;
    }

    public function getInstallment()
    {
        return $this->installment;
    }

    public function setInstallment($installment)
    {
        $this->installment = $installment;

        return $this;
    }

    public function getPaymentCard()
    {
        return $this->payment_card;
    }

    public function getCardType()
    {
        $pan = str_replace(" ", "", $this->getPaymentCard()->getCardNumber());

        $CCNumber = trim($pan);
        $this->card_type = 'Unknown';
        foreach (self::$creditcardTypes as $card) {
            if (! in_array(strlen($CCNumber), $card['cardLength'])) {
                continue;
            }
            $prefixes = '/^(' . implode('|', $card['cardPrefix']) . ')/';
            if (preg_match($prefixes, $CCNumber) == 1) {
                $this->card_type = $card['Name'];
                break;
            }
        }

        return $this->card_type;
    }


    public function getBillingAddress()
    {
        return PaymentAddress::find($this->getBillingAddressId());
    }

    public function getShippingAddress()
    {
        return PaymentAddress::find($this->getShippingAddressId());
    }

    private function savePaymentCard()
    {
        /** @var PaymentCard $paymentCard */
        $paymentCard = $this->getPaymentCard();

        if($paymentCard){
            $paymentCard->save();

            $this->setPaymentCardId($paymentCard->getId());

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }


    public function __toString()
    {
        return encrypt(json_encode($this, JSON_UNESCAPED_UNICODE));
    }

    private function clearOld()
    {
        $orders = \Mediapress\ECommerce\Models\Order::where('order_number', $this->order_number)->where('status', '<', 5)->get();
        foreach ($orders as $order) {
            $items = $order->items;
            foreach ($items as $item) {
                $item->delete();
            }
            $order->delete();
        }
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info): void
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @param mixed $agreement
     */
    public function setAgreement($agreement): void
    {
        $this->agreement = $agreement;
    }

}
