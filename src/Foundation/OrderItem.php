<?php


namespace Mediapress\ECommerce\Foundation;


class OrderItem
{

    /*
    *   1     İşlem Bekliyor
    *   2     Ödendi
    *   3     Geçersiz
    *   4     İptal Edilmiş
    *   5     Kabul Edilmiş
    *   6     Kargoda
    *   7     Teslim Edilmiş
    *   8     Reddedilmiş
    *   9     Tamamlandı
    *   10    İptal Talebi
    *   11    Kargoda İade
    *   12    Kargo Yapılması Gecikmiş
    *   13    Kabul Edilmiş Ama Zamanında Kargoya Verilmemiş
    *   14    Teslim Edilmiş İade
    *   15    Tamamlandıktan Sonra İade
    */

    private $id;
    private $order_id;
    private $order_number;
    private $status = 1;
    private $virtual_item = "0";
    private $model_type = "";
    private $model_id = "";
    private $model_price_key = "";
    private $information = array();
    private $currency = "TRY";
    private $quantity = 0.00;
    private $sub_total = 0.00;
    private $tax = 0.00;
    private $total = 0.00;
    private $unit_price = 0.00;
    private $unit_tax = 0.00;
    private $base_currency = "TRY";
    private $base_unit_price = 0.00;
    private $base_unit_tax = 0.00;
    private $tax_rate = 0;
    private $tax_include = "0";
    private $campaign_discount = 0.00;
    private $grand_total = 0.00;
    private $custom_name = "";
    private $tag = "";
    private $criterias = array();

    public function __construct(CartItem $item)
    {
        foreach ($item->toArray() as $key => $val) {
            if(isset($this->{$key})) {
                $this->{$key} = $val;
            }
        }
    }

    public function saveToDB()
    {
        $temp = \Mediapress\ECommerce\Models\OrderItem::updateOrCreate(

            [
                "order_id" => $this->order_id,
                "order_number" => $this->order_number,
                "status" => $this->status,
                "virtual_item" => $this->virtual_item,
                "model_type" => (string) $this->model_type,
                "model_id" => $this->model_id,
                "model_price_key" => $this->model_price_key,
                "base_unit_price" => $this->base_unit_price,
                "base_unit_tax" => $this->base_unit_tax,
                "tax_rate" => $this->tax_rate,
                "tax_include" => $this->tax_include,
                "campaign_discount" => $this->campaign_discount,
                "custom_name" => $this->custom_name,
                "tag" => $this->tag,
                "base_currency" => $this->base_currency,
            ],
            [
                "currency" => $this->currency,
                "information" => $this->information,
                "quantity" => $this->quantity,
                "sub_total" => $this->sub_total,
                "tax" => $this->tax,
                "total" => $this->total,
                "unit_price" => $this->unit_price,
                "unit_tax" => $this->unit_tax,
                "criterias" => $this->criterias,
                "grand_total" => $this->grand_total,
            ]
        );
        $this->setId($temp->id);
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }


    /**
     * @return int|null
     */
    public function getOrderId(): ?int
    {
        return $this->order_id;
    }


    /**
     * @param $order_id
     * @return $this
     */
    public function setOrderId($order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }


    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->order_number;
    }


    /**
     * @param $order_number
     * @return $this
     */
    public function setOrderNumber($order_number): self
    {
        $this->order_number = $order_number;

        return $this;
    }


    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }


    /**
     * @return int|null
     */
    public function getCartId(): ?int
    {
        return $this->cart_id;
    }

    /**
     * @return int|null
     */
    public function setCartId($cart_id): self
    {
        $this->cart_id = $cart_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getVirtualItem(): string
    {
        return $this->virtual_item;
    }

    /**
     * @param string $virtual_item
     */
    public function setVirtualItem($virtual_item): self
    {
        $this->virtual_item = $virtual_item;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getModelType(): ?string
    {
        return $this->model_type;
    }

    /**
     * @return int|null
     */
    public function getModelId(): ?int
    {
        return $this->model_id;
    }

    /**
     * @return string|null
     */
    public function getModelPriceKey(): ?string
    {
        return $this->model_price_key;
    }

    /**
     * @param string $model_price_key
     * @return $this
     */
    public function setModelPriceKey(string $model_price_key): self
    {
        $this->model_price_key = $model_price_key;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return array
     */
    public function getInformation(): array
    {
        return $this->information;
    }

    /**
     * @param array $information
     * @return $this
     */
    public function setInformation(array $information): self
    {
        $this->information = $information;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSubTotal(): ?float
    {
        return $this->sub_total;
    }

    /**
     * @param float $sub_total
     * @return $this
     */
    public function setSubTotal(float $sub_total): self
    {
        $this->sub_total = $sub_total;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTax(): ?float
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     * @return $this
     */
    public function setTax(float $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param float $total
     * @return $this
     */
    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getUnitPrice(): ?float
    {
        return $this->unit_price;
    }

    /**
     * @param float $unit_price
     * @return $this
     */
    public function setUnitPrice(float $unit_price): self
    {
        $this->unit_price = $unit_price;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getUnitTax(): ?float
    {
        return $this->unit_tax;
    }

    /**
     * @param float $unit_tax
     * @return $this
     */
    public function setUnitTax(float $unit_tax): self
    {
        $this->unit_tax = $unit_tax;

        return $this;
    }


    /**
     * @return string|null
     */
    public function getBaseCurrency(): ?string
    {
        return $this->base_currency;
    }

    /**
     * @param $base_currency
     * @return $this
     */
    public function setBaseCurrency(string $base_currency): self
    {
        $this->base_currency = $base_currency;

        return $this;
    }


    /**
     * @return float|null
     */
    public function getBaseUnitPrice(): ?float
    {
        return $this->base_unit_price;
    }


    /**
     * @param $base_unit_price
     * @return $this
     */
    public function setBaseUnitPrice($base_unit_price): self
    {
        $base_unit_price = (float)$base_unit_price;
        $this->base_unit_price = $base_unit_price;

        return $this;
    }


    /**
     * @return float|null
     */
    public function getBaseUnitTax(): ?float
    {
        return $this->base_unit_tax;
    }


    /**
     * @param $base_unit_tax
     * @return $this
     */
    public function setBaseUnitTax($base_unit_tax): self
    {
        $base_unit_tax = (float)$base_unit_tax;
        $this->base_unit_tax = $base_unit_tax;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTaxRate(): ?float
    {
        return $this->tax_rate;
    }


    /**
     * @param $tax_rate
     * @return $this
     */
    public function setTaxRate($tax_rate): self
    {
        $tax_rate = (float)$tax_rate;
        $this->tax_rate = $tax_rate;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTaxInclude(): ?string
    {
        return $this->tax_include;
    }


    /**
     * @param $tax_rate
     * @return $this
     */
    public function setTaxInclude($tax_include): self
    {
        $this->tax_include = $tax_include;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getCampaignDiscount(): ?float
    {
        return $this->campaign_discount;
    }

    /**
     * @param float $campaign_discount
     * @return $this
     */
    public function setCampaignDiscount(float $campaign_discount): self
    {
        $this->campaign_discount = $campaign_discount;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getGrandTotal(): ?float
    {
        return $this->grand_total;
    }


    /**
     * @param $grand_total
     * @return $this
     */
    public function setGrandTotal($grand_total): self
    {
        $this->grand_total = $grand_total;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomName(): ?string
    {
        return $this->custom_name;
    }

    /**
     * @param string $custom_name
     * @return $this
     */
    public function setCustomName(?string $custom_name): self
    {
        $this->custom_name = $custom_name;

        return $this;
    }


    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }


    /**
     * @param $tag
     * @return $this
     */
    public function setTag($tag): self
    {
        $this->tag = $tag;

        return $this;
    }


    /**
     * @return array
     */
    public function getCriterias(): array
    {
        return $this->criterias;
    }


    /**
     * @param array $criterias
     * @return $this
     */
    public function setCriterias(array $criterias): self
    {
        $this->criterias = $criterias;

        return $this;
    }


}
