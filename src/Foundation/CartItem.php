<?php


namespace Mediapress\ECommerce\Foundation;


class CartItem
{


    private $model;
    private $model_price;

    private $id;
    private $cart_id;
    private $virtual_item = "0";
    private $model_type;
    private $model_id;
    private $model_price_key;
    private $information = array();
    private $currency = "TRY";
    private $quantity = 0.00;
    private $sub_total = 0.00;
    private $tax = 0.00;
    private $total = 0.00;
    private $unit_price = 0.00;
    private $unit_tax = 0.00;
    private $base_currency = "TRY";
    private $base_unit_price = 0.00;
    private $base_unit_tax = 0.00;
    private $tax_rate = 0;
    private $tax_include = "0";
    private $campaign_discount = 0.00;
    private $grand_total = 0.00;
    private $tag;
    private $custom_name;
    private $criterias = array();


    public function saveToDB() {

        $temp = \Mediapress\ECommerce\Models\CartItem::updateOrCreate(
            [
                "cart_id" => $this->cart_id,
                "virtual_item" => $this->virtual_item,
                "model_type" => (string) $this->model_type,
                "model_id" => $this->model_id,
                "model_price_key" => $this->model_price_key,
                "base_unit_price" => $this->base_unit_price,
                "base_unit_tax" => $this->base_unit_tax,
                "tax_rate" => $this->tax_rate,
                "tax_include" => $this->tax_include,
                "campaign_discount" => $this->campaign_discount,
                "custom_name" => $this->custom_name,
                "tag" => $this->tag,
                "base_currency" => $this->base_currency,
            ],
            [
                "currency" => $this->currency,
                "information" => $this->information,
                "quantity" => $this->quantity,
                "sub_total" => $this->sub_total,
                "tax" => $this->tax,
                "total" => $this->total,
                "unit_price" => $this->unit_price,
                "unit_tax" => $this->unit_tax,
                "criterias" => $this->criterias,
                "grand_total" => $this->grand_total,
            ]
        );
        $this->setId($temp->id);
    }

    public function remove() {

        $item = \Mediapress\ECommerce\Models\CartItem::find($this->id);

        if($item) {
            $item->delete();
        }
    }

    public function taxInclude() {
        $temp = $this->base_unit_price;
        if($this->virtual_item == "0") {
            $temp = $this->model_price;
        }
        if($this->tax_include == "0") {
            $base_unit_tax = $this->model_price * $this->tax_rate;
            $this->setBaseUnitPrice($this->model_price);
            $this->setBaseUnitTax($base_unit_tax);
        } elseif($this->tax_include == "1") {

            $base_unit_price = $this->model_price / (1 + $this->tax_rate);
            $this->setBaseUnitPrice($base_unit_price);
            $this->setBaseUnitTax($base_unit_price * $this->tax_rate);
        }
    }

    /**
     * @return int|null
     */
    public function getId() : ?int {
        return $this->id;
    }
    /**
     * @param int $id
     * @return $this
     */
    public function setId($id) : self {
        $this->id = $id;

        return $this;
    }
    /**
     * @return int|null
     */
    public function getCartId() : ?int {
        return $this->cart_id;
    }
    /**
     * @return int|null
     */
    public function setCartId($cart_id) : self {
        $this->cart_id = $cart_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getVirtualItem() : string
    {
        return $this->virtual_item;
    }

    /**
     * @param string $virtual_item
     */
    public function setVirtualItem($virtual_item) : self
    {
        $this->virtual_item = $virtual_item;

        return $this;
    }

    /**
     * @return Model|null
     */
    public function getModel() {
        return $this->model;
    }

    /**
     * @param string $model_type
     * @param int $model_id
     * @return $this
     */
    public function setModel(string $model_type, int $model_id) : self {

        $this->model_type = $model_type;
        $this->model_id = $model_id;

        $temp_model = new $model_type;
        $this->model = $temp_model->find($model_id);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getModelType() : ?string {
        return $this->model_type;
    }

    /**
     * @return int|null
     */
    public function getModelId() : ?int {
        return $this->model_id;
    }

    /**
     * @return string|null
     */
    public function getModelPriceKey() : ?string {
        return $this->model_price_key;
    }

    /**
     * @param string $model_price_key
     * @return $this
     */
    public function setModelPriceKey(string $model_price_key = null) : self {
        if($model_price_key){
            $this->model_price_key = $model_price_key;
            if($this->model) {
                    $this->model_price = $this->model->$model_price_key;
                $this->setBaseUnitPrice($this->model_price);
            }
        }


        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency() : ?string {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency(string $currency) : self {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return array
     */
    public function getInformation() : array {
        return $this->information;
    }

    /**
     * @param array $information
     * @return $this
     */
    public function setInformation(array $information) : self {
        $this->information = $information;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity() : ?float {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(float $quantity) : self {
        $this->quantity = $quantity;
        $this->setUnitPrice($this->unit_price);
        $this->setUnitTax($this->unit_tax);
        $this->setCampaignDiscount($this->campaign_discount);

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSubTotal() : ?float {
        return $this->sub_total;
    }

    /**
     * @param float $sub_total
     * @return $this
     */
    public function setSubTotal(float $sub_total) : self {
        $this->sub_total = $sub_total;

        $temp_tax = $this->tax ?: 0;
        $this->setTotal($this->sub_total + $temp_tax);

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTax() : ?float {
        return $this->tax;
    }

    /**
     * @param float $tax
     * @return $this
     */
    public function setTax(float $tax) : self {
        $this->tax = $tax;

        $temp_sub_total = $this->sub_total ?: 0;
        $this->setTotal($this->tax + $temp_sub_total);

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotal() : ?float {
        return $this->total;
    }

    /**
     * @param float $total
     * @return $this
     */
    public function setTotal(float $total) : self {
        $this->total = $total;
        $this->setGrandTotal($total);

        return $this;
    }

    /**
     * @return float|null
     */
    public function getUnitPrice() : ?float {
        return $this->unit_price;
    }

    /**
     * @param float $unit_price
     * @return $this
     */
    public function setUnitPrice(float $unit_price) : self {
        $this->unit_price = $unit_price;

        $this->setSubTotal($this->quantity * $this->unit_price);

        return $this;
    }

    /**
     * @return float|null
     */
    public function getUnitTax() : ?float {
        return $this->unit_tax;
    }

    /**
     * @param float $unit_tax
     * @return $this
     */
    public function setUnitTax(float $unit_tax) : self {
        $this->unit_tax = $unit_tax;

        $this->setTax($this->quantity * $this->unit_tax);

        return $this;
    }


    /**
     * @return string|null
     */
    public function getBaseCurrency() : ?string
    {
        return $this->base_currency;
    }

    /**
     * @param $base_currency
     * @return $this
     */
    public function setBaseCurrency(string $base_currency) : self
    {
        $this->base_currency = $base_currency;
        $this->currency = $base_currency;

        return $this;
    }


    /**
     * @return float|null
     */
    public function getBaseUnitPrice() : ?float
    {
        return $this->base_unit_price;
    }


    /**
     * @param $base_unit_price
     * @return $this
     */
    public function setBaseUnitPrice($base_unit_price) : self
    {
        $base_unit_price = (float)$base_unit_price;
        $this->setUnitPrice($base_unit_price);
        $this->base_unit_price = $base_unit_price;
        $this->model_price = $base_unit_price;

        return $this;
    }


    /**
     * @return float|null
     */
    public function getBaseUnitTax() : ?float
    {
        return $this->base_unit_tax;
    }


    /**
     * @param $base_unit_tax
     * @return $this
     */
    public function setBaseUnitTax($base_unit_tax) : self
    {
        $base_unit_tax = (float)$base_unit_tax;
        $this->setUnitTax($base_unit_tax);
        $this->base_unit_tax = $base_unit_tax;


        return $this;
    }

    /**
     * @return float|null
     */
    public function getTaxRate() : ?float
    {
        return $this->tax_rate;
    }


    /**
     * @param $tax_rate
     * @return $this
     */
    public function setTaxRate($tax_rate) : self
    {

        $tax_rate = (float)$tax_rate;
        $this->tax_rate = $tax_rate;

        $this->taxInclude();

        return $this;
    }
    /**
     * @return float|null
     */
    public function getTaxInclude() : ?string
    {
        return $this->tax_include;
    }


    /**
     * @param $tax_rate
     * @return $this
     */
    public function setTaxInclude($tax_include) : self
    {
        $this->tax_include = $tax_include;
        $this->taxInclude();

        return $this;
    }

    /**
     * @return float|null
     */
    public function getCampaignDiscount() : ?float {
        return $this->campaign_discount;
    }

    /**
     * @param float $campaign_discount
     * @return $this
     */
    public function setCampaignDiscount(float $campaign_discount) : self {
        $this->campaign_discount = $campaign_discount;

        $this->grand_total = $this->total - ($this->total * $campaign_discount);

        return $this;
    }

    /**
     * @return float|null
     */
    public function getGrandTotal() : ?float
    {
        return $this->grand_total;
    }


    /**
     * @param $grand_total
     * @return $this
     */
    public function setGrandTotal($grand_total) : self
    {
        $this->grand_total = $grand_total;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomName() : ?string {
        return $this->custom_name;
    }

    /**
     * @param string $custom_name
     * @return $this
     */
    public function setCustomName(?string $custom_name) : self {
        $this->custom_name = $custom_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTag() : ?string
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag) : self
    {
        $this->tag = $tag;

        return $this;
    }


    /**
     * @return array
     */
    public function getCriterias() : array {
        return $this->criterias;
    }

    /**
     * @param array $criterias
     * @return $this
     */
    public function setCriterias(array $criterias) : self {
        $this->criterias = $criterias;

        return $this;
    }

    public function toArray()
    {
        $array = [];
        foreach ($this as $key=>$value){
            $array[$key] = $value;
        }

        return $array;
    }


}
