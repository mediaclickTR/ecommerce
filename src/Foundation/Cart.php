<?php


namespace Mediapress\ECommerce\Foundation;

use Mediapress\ECommerce\Models\Currency;

class Cart
{
    /**
     * =========================
     * DÖVİZ KODLARI
     * TRY [TÜRK LİRASI]
     * USD [AMERİKAN DOLARI]
     * AUD [AVUSTRALYA DOLARI]
     * DKK [DANİMARKA KRONU]
     * EUR [EURO]
     * GBP [İNGİLİZ STERLİNİ]
     * CHF [İSVİÇRE FRANGI]
     * SEK [İSVEÇ KRONU]
     * CAD [KANADA DOLARI]
     * KWD [KUVEYT DİNARI]
     * NOK [NORVEÇ KRONU]
     * SAR [SUUDİ ARABİSTAN RİYALİ]
     * JPY [JAPON YENİ]
     * BGN [BULGAR LEVASI]
     * RON [RUMEN LEYİ]
     * RUB [RUS RUBLESİ]
     * IRR [İRAN RİYALİ]
     * CNY [ÇİN YUANI]
     * PKR [PAKİSTAN RUPİSİ]
     * =========================
     */

    private $id;
    private $order_id;
    private $user_id;
    private $session_key = "mediapress.ecommerce.cart";
    private $session_id = "";
    private $quantity = 0.00;
    private $sub_total = 0.00;
    private $tax = 0.00;
    private $total = 0.00;
    private $campaign_discount = 0.00;
    private $coupon_id;
    private $grand_total = 0.00;
    private $currency = "TRY";
    private $information = [];

    public $items = array();

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        if (auth()->check()) {
            $this->user_id = auth()->id();
        }

        $session_key = session()->get($this->session_key);

        if (is_null($session_key)) {
            $this->session_id = $this->genId();
        } else {
            $this->session_id = $session_key;
        }
    }

    public function getItemByTag($tag)
    {
        foreach ($this->items as $item) {
            if ($item->getTag() == $tag) {
                return $item;
            }
        }
        return null;
    }

    public function makeOrder($order_number = null)
    {
        $order_number = $order_number ?: $this->genId();
        $order = new Order();
        if (config("ecommerce.transfer")){
            $currenciesData = getCurrencyData();
            if(!isset($currenciesData[$this->currency])){
                $this->currency = Currency::where('symbol',$this->currency)->first()->iso;
            }
        }


        $order = $order->setOrderNumber($order_number)
            ->setUserId($this->user_id)
            ->setCurrency($this->currency)
            ->setSubTotal($this->sub_total)
            ->setTax($this->tax)
            ->setTotal($this->total)
            ->setCampaignDiscount($this->campaign_discount)
            ->setGrandTotal($this->grand_total)
            ->addItems($this->items);

        return $order;
    }

    public function destroy(): void
    {

        $oldSessionId = session($this->session_key);
        session()->forget($this->session_key);


        if ($this->id) {
            \Mediapress\ECommerce\Models\Cart::delete($this->id);
            \Mediapress\ECommerce\Models\CartItem::where('cart_id', $this->id)
                ->delete();
        } else {
            $cart = \Mediapress\ECommerce\Models\Cart::whereSessionId($oldSessionId)->first();
            if(!$cart){
                if($this->getUserId()){
                    $carts = \Mediapress\ECommerce\Models\Cart::whereUserId($this->getUserId())->get();
                    foreach ($carts as $cart){
                        $this->removeCart($cart);
                    }
                }
            }
            if ($cart) {
                $this->removeCart($cart);
            }

        }
    }

    public function removeCart($cart){
        if($cart){
            \Mediapress\ECommerce\Models\CartItem::where('cart_id', $cart->id)->delete();
            $cart->delete();
        }
    }
    public function addItems($items): self
    {
        $this->quantity = 0;
        $this->sub_total = 0;
        $this->tax = 0;
        $this->total = 0;
        $this->grand_total = 0;
        /** @var CartItem $item */
        /** @var CartItem $cartItem */
        $list = [];
        $itemList = $this->items;
        $i = 500;
        foreach ($items as $key => $item) {
            foreach ($itemList as $hold => $cartItem) {
                if (
                    $item->getModelType() == $cartItem->getModelType() &&
                    $item->getModelId() == $cartItem->getModelId() &&
                    $item->getModelPriceKey() == $cartItem->getModelPriceKey() &&
                    floor($item->getBaseUnitPrice()) == floor($cartItem->getBaseUnitPrice())
                ) {
                    $cartItem->setQuantity($item->getQuantity() + $cartItem->getQuantity());
                    $list[$i++] = $cartItem;
                    unset($items[$key]);
                }else{
                    $list[$hold] = $cartItem;
                }
            }
        }
        foreach ($this->items as $item) {
            $item->remove();
        }
        foreach ($items as $item) {
            $list[] = $item;
        }
        foreach ($list as $item) {
            $this->quantity += $item->getQuantity();
            $this->sub_total += $item->getSubTotal();
            $this->tax += $item->getTax();
            $this->total += $item->getTotal();
            $this->grand_total += $item->getGrandTotal();
        }

        $this->items = $list;
        if ($this->total != $this->grand_total) {
            $this->campaign_discount = ($this->total - $this->grand_total) / $this->total;
        }
        return $this;
    }

    public function removeItems($item_ids = array()): self
    {


        /** @var CartItem $item */
        foreach ($this->items as $key => $item) {

            if (in_array($item->getId(), $item_ids)) {
                $item->remove();
                unset($this->items[$key]);
            }
        }
        $temp_items = $this->items;
        $this->clear();
        $this->addItems($temp_items);
        $this->save();
        return $this;
    }

    public function incItems($item_ids = array()): self
    {


        /** @var CartItem $item */
        foreach ($this->items as $key => $item) {
            if (in_array($item->getId(), $item_ids)) {
                $item->setQuantity($item->getQuantity() + 1);
            }
        }
        $temp_items = $this->items;
        $this->clear();
        $this->addItems($temp_items);
        $this->save();
        return $this;
    }

    public function decItems($item_ids = array()): self
    {


        /** @var CartItem $item */
        foreach ($this->items as $key => $item) {
            if (in_array($item->getId(), $item_ids)) {
                $quantitiy = $item->getQuantity() - 1;
                if ($quantitiy) {
                    $item->setQuantity($quantitiy);
                } else {
                    $item->remove();
                    unset($this->items[$key]);
                }

            }
        }
        $temp_items = $this->items;
        $this->clear();
        $this->addItems($temp_items);
        $this->save();
        return $this;
    }

    public function save(): self
    {
        $this->saveToDB();
        $this->saveItemToDB($this->items);

        session()->put($this->session_key, $this->session_id);

        return $this;
    }

    protected function saveToDB(): void
    {

        $cart = \Mediapress\ECommerce\Models\Cart::updateOrCreate(
            [
                "session_id" => $this->session_id,
            ],
            [
                "order_id" => $this->order_id,
                "user_id" => $this->user_id,
                "quantity" => $this->quantity,
                "sub_total" => $this->sub_total,
                "tax" => $this->tax,
                "total" => $this->total,
                "coupon_id" => $this->coupon_id,
                "grand_total" => $this->grand_total,
                "campaign_discount" => $this->campaign_discount,
                "currency" => $this->currency,
            ]
        );

        $this->id = $cart->id;
    }

    protected function saveItemToDB(&$items): void
    {
        $temp_items = [];
        foreach ($items as $item) {
            $item->setCartId($this->id);
            $item->saveToDB();

            $temp_items[$item->getId()] = $item;
        }
        $items = $temp_items;
    }

    protected function merge(Cart $cart): self
    {

        $cart->addItems($cart->items);
        $cart->setCurrency($this->currency);
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getOrderId(): ?int
    {
        return $this->order_id;
    }

    /**
     * @param $order_id
     * @return $this
     */
    public function setOrderId($order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * @param $user_id
     * @return $this
     */
    public function setUserId($user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSessionKey(): ?string
    {
        return $this->session_key;
    }

    /**
     * @param $session_id
     * @return $this
     */
    public function setSessionKey($session_key): self
    {
        $this->session_key = $session_key;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->session_id;
    }

    /**
     * @param $session_id
     * @return $this
     */
    public function setSessionId($session_id): self
    {
        $this->session_id = $session_id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param $quantity
     * @return $this
     */
    public function setCount($quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSubTotal(): ?float
    {
        return $this->sub_total;
    }

    /**
     * @param $sub_total
     * @return $this
     */
    public function setSubTotal($sub_total): self
    {
        $this->sub_total = $sub_total;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTax(): ?float
    {
        return $this->tax;
    }

    /**
     * @param $tax
     * @return $this
     */
    public function setTax($tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param $total
     * @return $this
     */
    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getCampaignDiscount(): ?float
    {
        return $this->campaign_discount;
    }

    /**
     * @param $campaign_discount
     * @return $this
     */
    public function setCampaignDiscount($campaign_discount): self
    {
        $this->campaign_discount = $campaign_discount;

        if($campaign_discount){
            $this->grand_total = $this->total - ($this->total / $campaign_discount);
        }


        return $this;
    }

    /**
     * @return int|null
     */
    public function getCouponId(): ?int
    {
        return $this->coupon_id;
    }

    /**
     * @param $coupon_id
     * @return $this
     */
    public function setCouponId($coupon_id): self
    {
        $this->coupon_id = $coupon_id;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getGrandTotal(): ?float
    {
        return $this->grand_total;
    }

    /**
     * @param $grand_total
     * @return $this
     */
    public function setGrandTotal($grand_total): self
    {
        $this->grand_total = $grand_total;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param $currency
     * @return $this
     */
    public function setCurrency($currency): self
    {
        $this->currency = $currency;

        $this->quantity = 0;
        $this->sub_total = 0;
        $this->tax = 0;
        $this->total = 0;
        $this->grand_total = 0;
        array_map(array($this, 'syncCurrency'), $this->items);

        return $this;
    }

    /**
     * @return array
     */
    public function getInformation(): array
    {
        return $this->information;
    }

    /**
     * @param array $information
     * @return $this
     */
    public function setInformation(array $information): self
    {
        $this->information = $information;

        return $this;
    }

    /**
     * @param CartItem $item
     * @return $this
     */
    protected function syncCurrency(&$item)
    {
        $target_currency_code = $this->currency;
        $self_currency_code = $item->getCurrency();

        if ($self_currency_code !== $target_currency_code) {

            $self_unit_price = $item->getUnitPrice();
            $self_unit_tax = $item->getUnitTax();

            try {
                $currenciesData = getCurrencyData();

                if(!isset($currenciesData[$target_currency_code])){
                    $currencyModel = Currency::where('symbol',$target_currency_code)->first();
                    if($currencyModel){
                        $target_currency_code = $currencyModel->iso;
                    }
                }
                $target_currency = $currenciesData[$target_currency_code];

                if(!isset($currenciesData[$self_currency_code])){
                    $currencyModel = Currency::where('symbol',$self_currency_code)->first();
                    if($currencyModel){
                        $self_currency_code = $currencyModel->iso;
                    }
                }
                $self_currency = $currenciesData[$self_currency_code];

                if ($self_currency["CurrencyName"] === "TRY") {
                    $currency_rate = 1 / $target_currency["BanknoteBuying"];
                } else {
                    $currency_rate = 1 / ((($target_currency["BanknoteBuying"] + $target_currency["BanknoteBuying"]) / 2)
                            / $self_currency["BanknoteBuying"]);
                }

            } catch (\Exception $e) {
                dd($e);
            }
            $item->setCurrency($target_currency_code);
            $item->setUnitTax($self_unit_tax * $currency_rate);
            $item->setUnitPrice($self_unit_price * $currency_rate);
            $item->setCampaignDiscount($item->getCampaignDiscount());
        }


        $this->quantity += $item->getQuantity();
        $this->sub_total += $item->getSubTotal();
        $this->tax += $item->getTax();
        $this->total += $item->getTotal();
        $this->grand_total += $item->getGrandTotal();
    }

    private function clear()
    {
        foreach ($this->items as $item) {
            $item->remove();
        }
        $this->setTotal(0);
        $this->setSubTotal(0);
        $this->setTax(0);
        $this->setCount(0);
        $this->setCampaignDiscount(0);
        $this->setGrandTotal(0);
        $this->items = [];
        return $this;
    }

    private function genId()
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPRSTUVXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring .= $characters[(rand(0, strlen($characters)) -1 )];
        }
        return $randstring;
    }
}
