<?php

Route::group(['prefix' => 'Orders', 'middleware' => 'panel.auth', 'as' => 'Orders.'], function () {
    Route::get('/', 'OrderController@index')->name('index');
    Route::get('ajax', 'OrderController@ajax')->name('ajax');
    Route::get('/show/{id}', 'OrderController@show')->name('show');
    Route::post('/update/{id}', 'OrderController@update')->name('update');
    Route::get('/delete/{id}', 'OrderController@delete')->name('delete');
    Route::get('/excel-export', 'OrderController@excel')->name('excel.export');
    Route::get('/updateStatus/{id}', 'OrderController@updateStatus')->name('updateStatus');
    Route::get('/updateOrderStatus/{id}', 'OrderController@updateOrderStatus')->name('updateOrderStatus');
});

Route::group(['prefix' => 'Price', 'middleware' => 'panel.auth', 'as' => 'Price.'], function () {
    Route::get('/', 'PriceController@index')->name('index');
    Route::get('ajax', 'PriceController@ajax')->name('ajax');
    Route::get('/show/{id}/{cg}', 'PriceController@show')->name('show');
    Route::post('/update/{id}', 'PriceController@update')->name('update');
    Route::get('/delete/{id}', 'PriceController@delete')->name('delete');
    Route::get('/get-variation/', 'PriceController@getVariations')->name('getvariations');
    Route::post('/add-variation/', 'PriceController@addVariation')->name('addvariation');
    Route::post('/edit-variation/', 'PriceController@editVariation')->name('editVariation');
    Route::get('/del-variation/', 'PriceController@removeVariation')->name('delvariations');

    Route::post('/import', 'PriceController@import')->name('import');
    Route::post('/export', 'PriceController@export')->name('export');
});

Route::group(['prefix' => 'Transfer', 'middleware' => 'panel.auth', 'as' => 'Transfer.'], function () {
    Route::get('/', 'TransferController@index')->name('index');
    Route::get('ajax', 'TransferController@ajax')->name('ajax');
    Route::get('/show/{id}', 'TransferController@show')->name('show');
    Route::get('/create', 'TransferController@create')->name('create');
    Route::post('/create', 'TransferController@store');
    Route::post('/update/{id}', 'TransferController@update')->name('update');
    Route::get('/delete/{id}', 'TransferController@delete')->name('delete');
});
