<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Entity\Models\User;

class CartItem extends Model
{
    use SoftDeletes;

    protected $table = 'cart_items';
    public $timestamps = true;

    protected $fillable = [
        "cart_id",
        "virtual_item",
        "model_type",
        "model_id",
        "model_price_key",
        "currency",
        "information",
        "quantity",
        "sub_total",
        "tax",
        "total",
        "unit_price",
        "unit_tax",
        "base_currency",
        "base_unit_price",
        "base_unit_tax",
        "tax_rate",
        "tax_include",
        "campaign_discount",
        "grand_total",
        "custom_name",
        "tag",
        "criterias"
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'information' => 'array',
        'criterias' => 'array'
    ];

    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id', 'id');
    }

    public function model() {
        return $this->morphTo();
    }

    public function getPriceAttribute() {
        $key = $this->model_price_key;

        return $this->model->$key;
    }
}
