<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Entity\Models\User;

class PaymentType extends Model
{
    use SoftDeletes;

    protected $table = 'payment_types';
    public $timestamps = true;

    protected $fillable = [
        "type",
        "bank",
        "detail",
        "provider"
    ];

    protected $casts = [
        'detail' => 'array'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

}
