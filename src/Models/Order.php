<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\ECommerce\Models\CartItem;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Entity\Models\User;

class Order extends Model
{

    protected $table = 'orders';
    public $timestamps = true;

    protected $fillable = [
        "order_number",
        "status",
        "order_status",
        "user_id",
        "payment_type_id",
        "payment_card_id",
        "billing_address_id",
        "shipping_address_id",
        "billing_address",
        "shipping_address",
        "currency",
        "sub_total",
        "tax",
        "total",
        "campaign_discount",
        "grand_total",
        "installment",
        "payed_total",
        "response",
        "info",
        "agreement",
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'billing_address' => 'array',
        'shipping_address' => 'array',
        'response' => 'array'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function items() {
        return $this->HasMany(OrderItem::class);
    }

    public function getStatusStringAttribute() {

        switch($this->status) {
            case 1:
                return langPart("order_status_1", "Ödeme Bekliyor");
            case 2:
                return langPart("order_status_2", "İşlemde");
            case 3:
                return langPart("order_status_3", "İptal");
            case 4:
                return langPart("order_status_4", "Geçersiz");
            case 5:
                return langPart("order_status_5", "Tamamlandı");
        }
    }
}
