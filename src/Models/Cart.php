<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\ECommerce\Models\CartItem;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Entity\Models\User;

class Cart extends Model
{
    use SoftDeletes;

    protected $table = 'carts';
    public $timestamps = true;

    protected $fillable = [
        "order_id",
        "user_id",
        "session_id",
        "ordered",
        "quantity",
        "sub_total",
        "tax",
        "total",
        "campaign_discount",
        "coupon_id",
        "grand_total",
        "currency",
        "information"
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'information' => 'array',
    ];

    public function session()
    {
        return session()->get($this->session_id);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function items()
    {
        return $this->HasMany(CartItem::class);
    }

    public function toFoundation()
    {
        $cart = new \Mediapress\ECommerce\Foundation\Cart();
        $items = $this->items;
        $hold_item = [];

        foreach ($items as $item) {
            $cartItem = new \Mediapress\ECommerce\Foundation\CartItem();
            $cartItem = $cartItem->setId($item->id)
                ->setCartId($this->id)
                ->setVirtualItem($item->virtual_item)
                ->setModel($item->model_type, $item->model_id)
                ->setModelPriceKey($item->model_price_key)
                ->setInformation($item->information)
                ->setTaxInclude($item->tax_include)
                ->setTaxRate($item->tax_rate)
                ->setQuantity($item->quantity)
                ->setBaseCurrency($item->currency)
                ->setBaseUnitPrice($item->base_unit_price)
                ->setBaseUnitTax($item->base_unit_tax)
                ->setCampaignDiscount($item->campaign_discount)
                ->setCustomName($item->custom_name)
                ->setTag($item->tag)
                ->setCriterias($item->criterias);

            $hold_item[] = $cartItem;
        }


        $cart = $cart->addItems($hold_item);
        $cart = $cart->setCurrency($this->currency);


        return $cart;
    }
}
