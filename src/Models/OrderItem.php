<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Entity\Models\User;

class OrderItem extends Model
{

    use SoftDeletes;

    protected $table = 'order_items';
    public $timestamps = true;

    protected $fillable = [
        "order_id",
        "order_number",
        "status",
        "virtual_item",
        "model_type",
        "model_id",
        "model_price_key",
        "currency",
        "information",
        "quantity",
        "sub_total",
        "tax",
        "total",
        "grand_total",
        "unit_price",
        "unit_tax",
        "base_currency",
        "base_unit_price",
        "base_unit_tax",
        "tax_rate",
        "tax_include",
        "campaign_discount",
        "custom_name",
        "tag",
        "criterias"
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = [
        'information' => 'array',
        'criterias' => 'array'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function getPriceAttribute()
    {
        $key = $this->model_price_key;

        return $this->model->$key;
    }

    public function getStatusStringAttribute() {

        switch($this->status) {
            case 1:
                return langPart("order_item_status_1", "İşlem Bekliyor");
            case 2:
                return langPart("order_item_status_2", "Ödendi");
            case 3:
                return langPart("order_item_status_3", "Geçersiz");
            case 4:
                return langPart("order_item_status_4", "İptal Edilmiş");
            case 5:
                return langPart("order_item_status_5", "Kabul Edilmiş");
            case 6:
                return langPart("order_item_status_6", "Kargoda");
            case 7:
                return langPart("order_item_status_7", "Teslim Edilmiş");
            case 8:
                return langPart("order_item_status_8", "Reddedilmiş");
            case 9:
                return langPart("order_item_status_9", "Tamamlandı");
            case 10:
                return langPart("order_item_status_10", "İptal Talebi");
            case 11:
                return langPart("order_item_status_11", "Kargoda İade");
            case 12:
                return langPart("order_item_status_12", "Kargo Yapılması Gecikmiş");
            case 13:
                return langPart("order_item_status_13", "Kabul Edilmiş Ama Zamanında Kargoya Verilmemiş");
            case 14:
                return langPart("order_item_status_14", "Teslim Edilmiş İade");
            case 15:
                return langPart("order_item_status_15", "Tamamlandıktan Sonra İade");
        }
    }
}
