<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Entity\Models\User;

class Currency extends Model
{
    use SoftDeletes;

    protected $table = 'currencies';
    public $timestamps = true;

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

}
