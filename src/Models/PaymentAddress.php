<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Entity\Models\User;

class PaymentAddress extends Model
{
    use SoftDeletes;

    protected $table = 'payment_addresses';
    public $timestamps = true;

    protected $fillable = [
        "user_id",
        "contact_name",
        "contact_email",
        "contact_phone",
        "address_title",
        "address",
        "region",
        "city",
        "country",
        "zip_code",
        "type",
        "corporate_name",
        "corporate_tax_number",
        "corporate_tax_office",
        "id_number",
        "name_surname",
        "ebill_payer",
        "cvar_1",
        "cvar_2",
        "ctex_1",
        "ctex_2"
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
