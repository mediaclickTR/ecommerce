<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Entity\Models\User;

class PaymentCard extends Model
{
    use SoftDeletes;

    protected $table = 'payment_cards';
    public $timestamps = true;

    protected $fillable = [
        "user_id",
        "card_holder_name",
        "card_number",
        "expire_month",
        "expire_year",
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

}
