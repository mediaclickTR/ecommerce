<?php

namespace Mediapress\ECommerce\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Entity\Models\User;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    public $timestamps = true;

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function variations(){
        return $this->hasMany(self::class, 'parent_id');
    }
}
