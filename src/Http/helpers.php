<?php

function cart()
{
    $db_cart = getUserCart();
    $session_cart = getSessionCart();

    if (!is_null($db_cart) && config("ecommerce.merge_previous_cart")) {
        if (!is_null($session_cart)) {
            if ($db_cart->id != $session_cart->id) {

                $session_cart->update(['user_id' => auth()->id()]);

                return $db_cart->toFoundation()->merge($session_cart->toFoundation());
            }
        }
        return $db_cart->toFoundation();
    } else if (!is_null($session_cart)) {
        return $session_cart->toFoundation();
    } else {
        return new \Mediapress\ECommerce\Foundation\Cart();
    }
}

function getUserCart()
{
    if (auth()->check()) {
        $temp = \Mediapress\ECommerce\Models\Cart::where('user_id', auth()->id())
            ->where('order_id', null)
            ->get();

        if($temp->count() > 1) {
            $temp->first()->delete();
            return $temp->last();
        }
        return $temp->first();
    }
}

function getSessionCart()
{
    if ($cart_session_id = session()->get("mediapress.ecommerce.cart")) {
        return \Mediapress\ECommerce\Models\Cart::where('session_id', $cart_session_id)
            ->where('order_id', null)
            ->first();
    }
}

function getCurrencyData()
{
    return \Illuminate\Support\Facades\Cache::remember('mediapress.currency.tcmb',60*60*2,function(){
        $currenciesData = [];
        $currencies = simplexml_load_file("https://www.tcmb.gov.tr/kurlar/today.xml");
        $currenciesData['TRY']['Unit'] = "1";
        $currenciesData['TRY']['Isim'] = 'TÜRK LİRASI';
        $currenciesData['TRY']['CurrencyName'] = 'TRY';
        $currenciesData['TRY']['ForexBuying'] = "1";
        $currenciesData['TRY']['ForexSelling'] = "1";
        $currenciesData['TRY']['BanknoteBuying'] = "1";
        $currenciesData['TRY']['BanknoteSelling'] = "1";

        foreach ($currencies as $group => $items) {
            $currencyCode = $items['CurrencyCode'];
            foreach ($items as $key => $item) {
                if (in_array($key, ["CrossRateUSD", "CrossRateOther"]))
                    continue;
                $currenciesData["$currencyCode"][$key] = "$item";
            }
        }

        return $currenciesData;
    });

}


