<?php

namespace Mediapress\ECommerce\Http\Controllers\Panel;


use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mediapress\ECommerce\Imports\PriceImport;
use Mediapress\ECommerce\Models\Currency;
use Mediapress\ECommerce\Models\Product;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\ECommerce\Exports\PriceExport;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Yajra\DataTables\Html\Builder;
use Html;
use DataTables;

class PriceController
{
    public function index()
    {
        $website = session('panel.website');
        $cg = [];
        $sitemaps = [];
        $details = $website->details;
        foreach ($details as $detail) {
            $cgModel = CountryGroup::find($detail->country_group_id);
            $cg[$cgModel->id] = $cgModel->title;
        }
        foreach (explode(',', config('ecommerce.price_menu')) as $id) {
            $sitemap = Sitemap::with('detail')->find($id);
            $sitemaps[$sitemap->id] = $sitemap->detail->getOriginal('name');

        }
        return view('ECommerceView::prices.index', compact('cg', 'sitemaps'));
    }

    public function show($id, $cg, Request $request)
    {
        $currencies = Currency::get(['id', 'iso'])->pluck('iso', 'id')->toArray();

        $sitemap = Sitemap::with(['detail' => function ($q) {
            $q->select('name', 'sitemap_id');
        }])->select('id')->find($id);
        $country = CountryGroup::find($cg)->list_name;
        $pages = Page::where('sitemap_id', $id)
            ->when($request->has('status') &&  $request->filled('status'), function($query) use($request){
                if( $request->status == 1 ){
                    return $query->where('status', 1);
                }else{
                    return $query->where('status','!=', 1);
                }
            })
            ->when($request->has('key') &&  $request->filled('key'), function($query) use($request){
                return $query->whereHas('detail', function($query) use ($request){
                    return $query->where('name', 'like', '%'.$request->key.'%');
                });
            })
            ->with(['detail' => function ($q) {
                $q->select('name', 'page_id');
            }])->orderBy('order')->select('id', 'status')->paginate(20);

        $products = [];
        foreach ($pages as $page) {
            $product = Product::where('page_id', $page->id)->where('country_group_id', $cg)->where('parent_id', 0)->first();
            $products[] = [
                'id' => $page->id,
                'name' => $page->detail->name,
                'status' => $page->status,
                'ecommerce' => $product ? $product->toArray() : []
            ];
        }

        $categories = $sitemap->categories()
            ->where('status', 1)
            ->where('category_id', 0)
            ->with('detail')
            ->orderBy('lft')
            ->get();
        return view('ECommerceView::prices.show', compact('sitemap', 'products', 'cg', 'country', 'currencies', 'categories', 'pages'));
    }

    public function update($id, Request $request)
    {

        Product::updateOrCreate([
            'page_id' => $request->id,
            'country_group_id' => $request->cg,
        ], [
            $request->name => $request->value
        ]);
    }

    public function getVariations(Request $request){
        $products = Product::where('parent_id', $request->id)->get();
        $output = "";
        if( $products->isNotEmpty() ){
            $output .= "<table class='table table-striped variationListTable'>";
            $output .= "<thead>";
            $output .= "<tr>";
            $output .= "<th>Varvasyon Adı</th>";
            $output .= "<th>Stok</th>";
            $output .= "<th>Ürün Kodu</th>";
            $output .= "<th>Liste Fiyatı</th>";
            $output .= "<th>Satış Fiyatı</th>";
            $output .= "<th>İşlemler</th>";
            $output .= "</tr>";
            $output .= "</thead>";
            $output .= "<tbody>";
            foreach( $products as $product ){
                $output .= "<tr>";
                $output .= "<td><input name='variation_name' class='form-control form-control-sm' value='".$product->variation_name."' /></td>";
                $output .= "<td><input name='stock' class='form-control form-control-sm' value='".$product->stock."' /></td>";
                $output .= "<td><input name='code' class='form-control form-control-sm' value='".$product->code."' /></td>";
                $output .= "<td><input name='price' class='form-control form-control-sm' value='".$product->price."' /></td>";
                $output .= "<td><input name='sale_price' class='form-control form-control-sm' value='".$product->sale_price."' /></td>";
                $output .= "<td style='width:130px;'><button class='btn btn-sm btn-danger d-inline-block mr-3 delVariation' data-id='".$product->id."'><i class='fas fa-trash'></i></button><button class='btn btn-sm  d-inline-block btn-warning editVariation' data-id='".$product->id."'><i class='fas fa-edit'></i></button></td>";
                $output .= "</tr>";
            }
            $output .= "</tbody></table>";
            return response()->json([
                'status' => 1,
                'render' => $output
            ]);
        }else{
            return response()->json([
                'status' => 0
            ]);
        }
    }

    public function addVariation(Request $request){
        $product = Product::find($request->id);
        $insert_data = [
            'page_id' => $product->page_id,
            'country_group_id' => $product->country_group_id,
            'stock' => $request->stock,
            'code' => $request->code,
            'tax' => $product->tax,
            'price' => $request->price,
            'sale_price' => $request->sale_price,
            'installment' => $product->installment,
            'currency_id' => $product->currency_id,
            'variation_name' => $request->variation_name,
            'parent_id' => $product->id
        ];
        Product::create($insert_data);
        return response()->json([
            'status' => 1
        ]);
    }

    public function editVariation(Request $request){
        $update_date = [
            'stock' => $request->stock,
            'code' => $request->code,
            'price' => $request->price,
            'sale_price' => $request->sale_price,
            'variation_name' => $request->variation_name
        ];
        Product::where('id', $request->id)->update($update_date);
        return response()->json([
            'status' => 1
        ]);
    }

    public function removeVariation(Request $request){
        if( $request->id ){
            $product = Product::where('id', $request->id)->first();
            $parent_id = $product->parent_id;
            $product->forceDelete();
            return response()->json([
                'status' => 1,
                'parent_id' => $parent_id
            ]);
        }
    }

    public function export(Request $request)
    {

        $currentCategory = Category::findOrFail($request->get('category_id'));
        $categoryIds = Category::where('sitemap_id', $request->get('sitemap_id'))
            ->where('lft', '<=', $currentCategory->lft)
            ->where('rgt', '>=', $currentCategory->rgt)
            ->pluck('id')
            ->toArray();

        $pageIds = Page::whereHas('categories', function ($q) use ($categoryIds) {
            $q->whereIn('id', $categoryIds);
        })
            ->pluck('id')
            ->toArray();

        $products = Product::whereIn('page_id', $pageIds)->where('country_group_id', $request->get('cg'))->get();

        return Excel::download(new PriceExport($products), 'price.xlsx');
    }

    public function import(Request $request) {

        $file = $request->file('excel');

        if($file) {
            $fileName = $file->store(null, 'public');
            $fileName = storage_path('app/public/'. $fileName);


            Excel::import(new PriceImport(), $fileName);

            @unlink($fileName);

            return back();
        }
    }
}
