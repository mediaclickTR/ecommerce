<?php

namespace Mediapress\ECommerce\Http\Controllers\Panel;


use Illuminate\Http\Request;
use Mediapress\ECommerce\DataTable\Tables\Banks;
use Mediapress\ECommerce\DataTable\Tables\Orders;
use Mediapress\ECommerce\Foundation\Banks\Transfer;
use Mediapress\ECommerce\Models\PaymentType;
use Mediapress\Http\Controllers\PanelController;
use Yajra\DataTables\Html\Builder;

class TransferController extends PanelController
{


    public function index(Banks $banks, Builder $builder)
    {
        $dataTable = $banks->columns($builder)->ajax(route('Transfer.ajax'));
        return view('ECommerceView::transfer.index', compact('dataTable'));
    }

    public function show($id)
    {
        $bank = PaymentType::find($id);
        return view('ECommerceView::transfer.edit', compact("bank"));
    }

    public function create()
    {
        return view('ECommerceView::transfer.create');
    }

    public function ajax()
    {
        return app(Banks::class)->ajax();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|integer',
            'iban' => 'required'
        ]);
        $code = $request->get('code');
        $iban = $request->get('iban');
        PaymentType::create([
            'type' => 'transfer',
            'bank' => config('ecommerce.banks.' . $code),
            'provider' => Transfer::class,
            'detail' => [
                'code' => $code,
                'iban' => $iban
            ]
        ]);
        return redirect(route('Transfer.index'));

    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'code' => 'required|integer',
            'iban' => 'required'
        ]);
        $code = $request->get('code');
        $iban = $request->get('iban');
        PaymentType::where('id', $id)->update([
            'type' => 'transfer',
            'bank' => config('ecommerce.banks.' . $code),
            'provider' => Transfer::class,
            'detail' => [
                'code' => $code,
                'iban' => $iban
            ]
        ]);
        return redirect(route('Transfer.index'));

    }

    public function delete($id)
    {

        PaymentType::where('id', $id)->delete();
        return redirect(route('Transfer.index'));

    }


}
