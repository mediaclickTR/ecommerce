<?php

namespace Mediapress\ECommerce\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Mediapress\ECommerce\DataTable\Tables\Orders;
use Mediapress\ECommerce\Foundation\OrderExport;
use Mediapress\ECommerce\Models\Order;
use Yajra\DataTables\Html\Builder;
use Html;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;

class OrderController
{


    public function index(Orders $orders, Builder $builder)
    {
        $dataTable = $orders->columns($builder)->ajax(route('Orders.ajax'));
        return view('ECommerceView::orders.index', compact('dataTable'));
    }

    public function show($id)
    {
        $order = Order::with(['items', 'user'])->find(decrypt($id));
        $this->checkECommerceFunctions();
        $user = getUserOfOrder($order);
        $items = getItemsOfOrder($order);
        $total = getTotalOfOrder($order);
        $extra_data = [];
        if( function_exists('set_extra_data_order') ){
            $extra_data = set_extra_data_order($order->order_number);
        }
        return view('ECommerceView::orders.show', compact("order", 'items', 'user', 'total', 'extra_data'));
    }

    public function ajax()
    {
        if (method_exists('\App\DataTable\Orders', 'columns')) {
            return app('\App\DataTable\Orders')->ajax();
        }
        return app('Mediapress\ECommerce\DataTable\Tables\\' . class_basename(Orders::class))->ajax();
    }

    public function delete($id)
    {
        $order = Order::find(decrypt($id));
        if ($order->delete()) {
            foreach ($order->items as $item) {
                $item->delete();
            }
            return redirect()->back()->with('success', trans('ECommercePanel::general.deleteOption.success'));
        }
        return redirect()->back()->with('error', trans('ECommercePanel::general.deleteOption.error'));
    }

    private function checkECommerceFunctions()
    {
        foreach (config('ecommerce.functions') as $key => $value) {
            if (!function_exists($key)) {
                throw new $value($key . ' not found');
            }
        }
    }

    public function excel()
    {
        $this->checkECommerceFunctions();
        $export = new OrderExport();
        return Excel::download($export, 'orders.xlsx');
    }

    public function updateStatus(int $id, Request $request)
    {
        $order = Order::find($id);

        if($order) {
            $order->update(['status' => $request->get('status')]);

            return response()->json('OK');
        }
        return response()->json('ERROR');
    }

    public function updateOrderStatus(int $id, Request $request){
        $order = Order::find($id);

        if($order) {
            $order->update(['order_status' => $request->get('status')]);

            return response()->json('OK');
        }
        return response()->json('ERROR');
    }
}
