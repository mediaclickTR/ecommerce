<?php

namespace Mediapress\ECommerce;

use Mediapress\Models\MPModule;

class ECommerce extends MPModule
{
    public $name = "ECommerce";
    public $url = "mp-admin/ECommerce";
    public $description = "ECommerce";
    public $author = "";
    public $menus = [];
    public $plugins = [];


    public function fillMenu($menu)
    {

        if (config('ecommerce.show_menu')) {
            #region Header Menu > Settings > Panel Users > Set

            // Tab Name Set
            data_set($menu, 'header_menu.content.cols.ecommerce.name', trans("ECommercePanel::module_info.name"));

            $settings_cols_panel_users_rows_set = [
                [
                    "type" => "submenu",
                    "title" => trans("ECommercePanel::menu_titles.name"),
                    "url" => route("Orders.index")
                ]
            ];

            $menu = dataGetAndMerge($menu, 'header_menu.content.cols.ecommerce.rows',$settings_cols_panel_users_rows_set);
            #endregion

        }

        if (config('ecommerce.price_menu')) {
            #region Header Menu > Settings > Panel Users > Set

            // Tab Name Set
            data_set($menu, 'header_menu.content.cols.ecommerce.name', trans("ECommercePanel::module_info.name"));

            $settings_cols_panel_users_rows_set = [
                [
                    "type" => "submenu",
                    "title" => trans("ECommercePanel::menu_titles.price"),
                    "url" => route("Price.index")
                ]
            ];

            $menu = dataGetAndMerge($menu, 'header_menu.content.cols.ecommerce.rows',$settings_cols_panel_users_rows_set);
            #endregion

        }

        if (config('ecommerce.transfer')) {
            #region Header Menu > Settings > Panel Users > Set

            // Tab Name Set
            data_set($menu, 'header_menu.content.cols.ecommerce.name', trans("ECommercePanel::module_info.name"));

            $settings_cols_panel_users_rows_set = [
                [
                    "type" => "submenu",
                    "title" => trans("ECommercePanel::menu_titles.transfer"),
                    "url" => route("Transfer.index")
                ]
            ];

            $menu = dataGetAndMerge($menu, 'header_menu.content.cols.ecommerce.rows',$settings_cols_panel_users_rows_set);
            #endregion

        }


        return $menu;

    }
}
