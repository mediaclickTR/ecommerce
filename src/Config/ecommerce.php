<?php

use Mediapress\ECommerce\Exceptions\ExcelHeadException;
use Mediapress\ECommerce\Exceptions\ItemException;
use Mediapress\ECommerce\Exceptions\TotalException;
use Mediapress\ECommerce\Exceptions\UserException;

return [
    "merge_previous_cart" => env("MEDIAPRESS_ECOMMERCE_MERGE", true),
    "show_menu" => env("MEDIAPRESS_ECOMMERCE_MENU", false),
    "get_user" => env("MEDIAPRESS_ECOMMERCE_USER", false),
    "price_menu" => env("MEDIAPRESS_ECOMMERCE_PRICE", false),
    "transfer" => env("MEDIAPRESS_ECOMMERCE_TRANSFER", false),
    "functions" => [
        'getUserOfOrder' => UserException::class,
        'getItemsOfOrder' => ItemException::class,
        'getTotalOfOrder' => TotalException::class,
        'getExcelTitleRow' => ExcelHeadException::class,
    ],
    "banks" => [
        10 => 'Ziraat Bankası',
        12 => 'Halkbank',
        15 => 'Vakıfbank',
        32 => 'Teb',
        46 => 'Akbank',
        59 => 'Şekerbank',
        62 => 'Garanti BBVA',
        64 => 'İş Bankası',
        67 => 'Yapıkredi',
        71 => 'Dışbank',
        92 => 'Citibank',
        96 => 'Turkish Bank',
        99 => 'Ing Bank',
        103 => 'Fibabanka',
        108 => 'Mng Bank',
        109 => 'ICBC',
        111 => 'QNB Finansbank',
        123 => 'HSBC',
        124 => 'Alternatif Bank',
        125 => 'Tekfen Bank',
        134 => 'Denizbank',
        135 => 'Anadolu Bank',
        203 => 'Albaraka Türk',
        205 => 'Kuveyt Türk',
        206 => 'Sağlam Bank',
    ],
    "excel" => [
        "export" => env('MEDIAPRESS_ECOMMERCE_EXPORT_EXCEL', false),
    ]

];
