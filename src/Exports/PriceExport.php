<?php

namespace Mediapress\ECommerce\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class PriceExport implements FromArray
{
    private $products;

    public function __construct($products)
    {
        $this->products = $products;
    }
    public function array(): array
    {
        $array = [];
        $array[] = ['id', 'Ürün Kodu', 'Ürün Adı', 'Vergi Oranı', 'Liste Fiyatı', 'Satış Fiyatı'];

        foreach ($this->products as $product) {
            $array[] = [
                'id' => $product->id,
                'code' => $product->code,
                'name' => $product->page->detail->name,
                'tax' => $product->tax,
                'price' => $product->price,
                'sale_price' => $product->sale_price,
            ];
        }

        return $array;
    }
}
