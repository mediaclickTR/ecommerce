<?php

namespace Mediapress\ECommerce\DataTable\Tables;

use Carbon\Carbon;
use Html;
use Mediapress\ECommerce\Models\Order;
use Mediapress\ECommerce\Models\PaymentType;
use Yajra\DataTables\Facades\DataTables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;

class Banks extends DataTables
{
    const DATA = 'data';
    const NAME = 'name';
    const TITLE = 'title';
    const FOOTER = 'footer';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function columns(Builder $builder)
    {
        $request = request();
        return $builder->parameters([
            "lengthMenu" => [25, 50, 75, 100],
            "pageLength" => session('pageLength.sitemaps.' . $request->segments()[count($request->segments()) - 1], 25)
        ])->columns([
            [
                self::DATA => 'id',
                self::NAME => 'id',
                self::TITLE => 'ID',
                self::FOOTER => 'Id',
            ],
            [
                self::DATA => 'name',
                self::NAME => 'name',
                self::TITLE => 'Banka',
                self::FOOTER => 'Banka',
            ],
            [
                self::DATA => 'logo',
                self::NAME => 'logo',
                self::TITLE => 'Logo',
                self::FOOTER => 'Logo'
            ],
            [
                self::DATA => 'iban',
                self::NAME => 'iban',
                self::TITLE => 'IBAN',
                self::FOOTER => 'IBAN'
            ],


        ])->addAction([self::TITLE => trans("MPCorePanel::general.actions")]);
    }

    public function ajax()
    {
        $request = request();
        $url = parse_url($request->header('referer'));
        if (isset($url['query'])) {
            parse_str('&' . $url['query'], $array);
        }

        $banks = PaymentType::where('type','transfer');



        return Datatables::eloquent($banks)
            ->addColumn('sort', function ($bank) {
                return $bank->id;
            })

            ->editColumn('name', function ($bank) {
              return '<a href="' . route('Transfer.show', ['id' => $bank->id]) . '"  title="' . trans("ECommercePanel::general.show") . '" >'.$bank->bank.'</a>';

            })
            ->addColumn('logo', function ($bank) {
                return '<img src="/assets/images/bl/'.$bank->detail['code'].'.png">';
            })
            ->editColumn('iban', function ($bank) {
                return $bank->detail['iban'];
            })

            ->addColumn('action', function ($bank) {
                $showButton = '<a href="' . route('Transfer.show', ['id' => $bank->id]) . '"  title="' . trans("ECommercePanel::general.show") . '" ><span class="fa fa-eye"></span></a>';
                $deleteButton = '<a href="' . route('Transfer.delete', ['id' => $bank->id]) . '" class="needs-confirmation" title="' . trans("ECommercePanel::general.delete") . '" data-dialog-text="' . trans("MPCorePanel::general.action_cannot_undone") . '" data-dialog-cancellable="1" data-dialog-type="warning" ><span class="fa fa-trash"></span></a>';
                return $showButton. $deleteButton;
            })->rawColumns([
                "name", "action", "logo", 'iban'
            ])
            ->setRowId('tbl-{{$id}}')->make(true);

    }
}
