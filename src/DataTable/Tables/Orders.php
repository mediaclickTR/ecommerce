<?php

namespace Mediapress\ECommerce\DataTable\Tables;

use Carbon\Carbon;
use Html;
use Mediapress\ECommerce\Models\Order;
use Yajra\DataTables\Facades\DataTables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;

class Orders extends DataTables
{
    const DATA = 'data';
    const NAME = 'name';
    const TITLE = 'title';
    const FOOTER = 'footer';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function columns(Builder $builder)
    {
        $request = request();
        return $builder->parameters([
            "lengthMenu" => [25, 50, 75, 100],
            "pageLength" => session('pageLength.sitemaps.' . $request->segments()[count($request->segments()) - 1], 25)
        ])->columns([
            [
                self::DATA => 'sort',
                self::NAME => 'sort',
                self::TITLE =>  trans('ECommercePanel::general.sort'),
                self::FOOTER => trans('ECommercePanel::general.sort'),
            ],
            [
                self::DATA => 'status',
                self::NAME => 'status',
                self::TITLE => '<i class="fa fa-info-circle"></i>',
                self::FOOTER => '<i class="fa fa-info-circle"></i>',
            ],
            [
                self::DATA => 'order_name',
                self::NAME => 'order_name',
                self::TITLE => trans("ECommercePanel::general.order_name"),
                self::FOOTER => trans("ECommercePanel::general.order_name")
            ],
            [
                self::DATA => 'order_number',
                self::NAME => 'order_number',
                self::TITLE => trans("ECommercePanel::general.order_number"),
                self::FOOTER => trans("ECommercePanel::general.order_number")
            ],
            [
                self::DATA => 'items',
                self::NAME => 'items',
                self::TITLE => trans("ECommercePanel::general.total_items"),
                self::FOOTER => trans("ECommercePanel::general.total_items")
            ],
            [
                self::DATA => 'total',
                self::NAME => 'total',
                self::TITLE => trans("ECommercePanel::general.total"),
                self::FOOTER => trans("ECommercePanel::general.total")
            ],
            [
                self::DATA => 'currency',
                self::NAME => 'currency',
                self::TITLE => trans("ECommercePanel::general.currency"),
                self::FOOTER => trans("ECommercePanel::general.currency")
            ],
            [
                self::DATA => 'payment_type_id',
                self::NAME => 'payment_type_id',
                self::TITLE => trans("ECommercePanel::general.payment_type_text"),
                self::FOOTER => trans("ECommercePanel::general.payment_type_text")
            ],
            [
                self::DATA => 'created_at',
                self::NAME => 'created_at',
                self::TITLE => trans("ECommercePanel::general.order_date"),
                self::FOOTER => trans("ECommercePanel::general.order_date")
            ],

        ])->addAction([self::TITLE => trans("MPCorePanel::general.actions")]);
    }

    public function ajax()
    {
        $request = request();
        $array = [];
        $url = parse_url($request->header('referer'));
        if (isset($url['query'])) {
            parse_str('&' . $url['query'], $array);
        }

        $orders = Order::select("orders.id", "orders.order_number", "orders.grand_total", "orders.status", "orders.user_id",
            "orders.payment_type_id", "orders.total", "orders.currency", "orders.campaign_discount", "orders.billing_address",
            self::CREATED_AT, self::UPDATED_AT)
            ->where('order_status', 1)
            ->when(isset($array['key']) && !empty($array['key']), function($query) use ($array){
                return $query->whereRaw('concat(order_number," ",billing_address) like ?', "%{$array['key']}%");
            })
            ->when(isset($array['status']) && !empty($array['status']), function($query) use ($array){
                return $query->where('status', $array['status']);
            })
            ->when(isset($array['min_price']) && !empty($array['min_price']), function($query) use ($array){
                return $query->where('grand_total', '>=', $array['min_price']);
            })
            ->when(isset($array['max_price']) && !empty($array['max_price']), function($query) use ($array){
                return $query->where('grand_total', '<=', $array['max_price']);
            })
            ->when(isset($array['payment_type']) && !empty($array['payment_type']), function($query) use ($array){
                return $query->where('payment_type_id', $array['payment_type']);
            })
            ->when(isset($array['start_date']) && !empty($array['start_date']), function($query) use ($array){
                return $query->whereDate('created_at', '>=', $array['start_date']);
            })
            ->when(isset($array['end_date']) && !empty($array['end_date']), function($query) use ($array){
                return $query->whereDate('created_at', '<=', $array['end_date']);
            })
            ->with('items')
            ->orderByDesc('created_at')
            ->distinct('orders.id');


        return Datatables::eloquent($orders)
            ->addColumn('sort', function ($order) {
                return $order->id;
            })
            ->editColumn('status', function ($order) {
                /*
                  *   1     Ödeme Bekliyor
                  *   2     İşlemde
                  *   3     İptal
                  *   4     Geçersiz
                  *   5     Kargoya Verildi
                  *   6     Tamamlandı
                  */
                switch ($order->status) {
                    case 1:
                        return '<i class="far fa-hourglass text-warning" title="'.trans("ECommercePanel::general.status.payment_waiting").'"></i>';
                    case 2:
                        return '<i class="far fa-credit-card text-info" title="'.trans("ECommercePanel::general.status.process").'"></i>';
                    case 3:
                        return '<i class="fas fa-times text-danger" title="'.trans("ECommercePanel::general.status.cancel").'"></i>';
                    case 4:
                        return '<i class="fas fa-ban text-danger" title="'.trans("ECommercePanel::general.status.invalid").'"></i>';
                    case 5:
                        return '<i class="fas fa-shipping-fast text-info" title="'.trans("ECommercePanel::general.status_shipping").'"></i>';
                    case 6:
                        return '<i class="fas fa-check text-success" title="'.trans("ECommercePanel::general.status.success").'"></i>';
                }
            })
            ->addColumn('order_name', function ($order) {
                return $order->billing_address['contact_name'];

            })
            ->editColumn('order_number', function ($order) {
                return '<a href="' . route('Orders.show', ['id' => encrypt($order->id)]) . '"  title="' . trans("ECommercePanel::general.show") . '" >'.$order->order_number.'</a>';

            })
            ->addColumn('items', function ($order) {
                return $order->items->count();
            })
            ->editColumn('total', function ($order) {
                return $order->grand_total;
            })
            ->editColumn('currency', function ($order) {
                return $order->currency;
            })
            ->editColumn('payment_type_id', function ($order) {
                if( $order->payment_type_id == 1 ){
                    return trans("ECommercePanel::general.payment_credit");
                }else{
                    return trans("ECommercePanel::general.transfer");
                }
            })
            ->editColumn('created_at', function ($order) {
                return $order->created_at;
            })
            ->addColumn('action', function ($order) {
                $showButton = '<a href="' . route('Orders.show', ['id' => encrypt($order->id)]) . '"  title="' . trans("ECommercePanel::general.show") . '" ><span class="fa fa-eye"></span></a>';
//                $deleteButton = '<a href="' . route('Orders.delete', ['id' => encrypt($order->id)]) . '" class="needs-confirmation" title="' . trans("ECommercePanel::general.delete") . '" data-dialog-text="' . trans("MPCorePanel::general.action_cannot_undone") . '" data-dialog-cancellable="1" data-dialog-type="warning" ><span class="fa fa-trash"></span></a>';
                return $showButton/*. $deleteButton*/;
            })->rawColumns([
                "status", "action", "sort", 'order_number'
            ])
            ->setRowId('tbl-{{$id}}')->make(true);

    }
}
