<?php

namespace Mediapress\ECommerce\Facades;

use Illuminate\Support\Facades\Facade;

class ECommerce extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'ECommerce';
    }

}
