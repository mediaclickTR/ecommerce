<?php

namespace Mediapress\ECommerce\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Mediapress\ECommerce\Models\Product;
use Mediapress\Modules\MPCore\Models\CountryGroup;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\LanguagePart;
use Illuminate\Support\Collection;

class PriceImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $rows = $rows->toArray();

        foreach ($rows as $mainKey => $row)
        {
            if($mainKey == 0) {
                continue;
            }

            $product = Product::find($row[0]);

            if(!$product) {
                continue;
            }

            $product->update([
                'tax' => $row[3],
                'price' => $row[4],
                'sale_price' => $row[5]
            ]);
        }
    }
}
