<?php

namespace Mediapress\ECommerce\Exceptions;

use Exception;
use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;

class ItemException extends Exception implements ProvidesSolution
{

    public function getSolution(): Solution
    {
        return BaseSolution::create('`getItemsOfOrder` function is not found')
            ->setSolutionDescription('create `getItemsOfOrder($order)` function at your `helpers.php` and return [items => [key=>value]] for table');

    }
}
