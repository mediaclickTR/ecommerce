<?php

namespace Mediapress\ECommerce\Exceptions;

use Exception;
use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;

class ExcelHeadException extends Exception implements ProvidesSolution
{

    public function getSolution(): Solution
    {
        return BaseSolution::create('`getExcelTitleRow` function is not found')
            ->setSolutionDescription('create `getExcelTitleRow()` function at your `helpers.php` and return array for table');

    }
}
