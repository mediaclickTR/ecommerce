<?php

namespace Mediapress\ECommerce\Exceptions;

use Exception;
use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;

class TotalException extends Exception implements ProvidesSolution
{

    public function getSolution(): Solution
    {
        return BaseSolution::create('`getTotalOfOrder` function is not found')
            ->setSolutionDescription('create `getTotalOfOrder($order)` function at your `helpers.php` and return key=>value for table');

    }
}
