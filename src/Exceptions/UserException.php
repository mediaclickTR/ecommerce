<?php

namespace Mediapress\ECommerce\Exceptions;

use Exception;
use Facade\IgnitionContracts\BaseSolution;
use Facade\IgnitionContracts\ProvidesSolution;
use Facade\IgnitionContracts\Solution;

class UserException extends Exception implements ProvidesSolution
{

    public function getSolution(): Solution
    {
        return BaseSolution::create('`getUserOfOrder` function is not found')
            ->setSolutionDescription('create `getUserOfOrder($order)` function at your `helpers.php` and return key=>value for table');

    }
}
